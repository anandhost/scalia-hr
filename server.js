// Hostmud Cloud Solutions Private Limited
// server.js

// set up ======================================================================
// get all the tools we need
const newrelic = require('newrelic');
const express  = require('express');
const app      = express();
const port     = process.env.PORT || 8090;
const mongoose = require('mongoose');
const passport = require('passport');
const flash    = require('connect-flash');

const morgan = require('morgan');
const moment = require('moment');
const schedule = require('node-schedule');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const fileUpload = require('express-fileupload');

const User = require('./app/models/user');
const configDB = require('./config/database.js');
const NDClient = require('./app/models/NDClient');
const NDBilling = require('./app/models/NDBilling');

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());//to upload files
app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({
    secret: 'somerandomsecrettogeneratesessions', // session secret
    resave: true,
    saveUninitialized: true,
    rolling: true,
    cookie : {maxAge: 900000}
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
// Add headers
app.disable('x-powered-by');
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

let NDBillingRecurrence = schedule.scheduleJob('0 0 1 * *', function(){
  console.log('This runs on the start of every month');
  NDBilling.find({occurrence : "Reccurring", startDate : { $gte : moment().subtract(1,'months').startOf('month'), $lte : moment().subtract(1,'months').endOf('month')}}, (err, foundBillings) => {
    if(err){
      User.updateMany({permission : { $in : ['manager']}},{
        $push : {
          notification : {
            title : "An Error Occured Regenerating the bills. Contact IT Team.",
            date : Date.now(),
            icon : " mdi mdi-alert-decagram",
            color : "notify-icon bg-danger"
          }
        }
      }, (err, results)=> {
        if(err) {
          console.log(err);
        }
        else {
          console.log("Error notification sent to Managers");
        }
      })
    }
    foundBillings.forEach((bill) =>{
      //Insert billls in database using current start Date
    })
  })
});

//to disable back button from showing data after logout
app.use(function(req, res, next) {
  res.setHeader('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  next();
});
// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
