$(document).ready(function(){
  // validate signup form on keyup and submit
		$("#password-reset-form").validate({
			rules: {
				password: {
					required: true,
					minlength: 6
				},
				cnfpassword: {
					required: true,
					minlength: 5,
					equalTo: "#password"
				}
			},
			messages: {
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long"
				},
				cnfpassword: {
					required: "Please provide a password",
					minlength: "Your password must be at least 6 characters long",
					equalTo: "Please enter the same password as above"
				}
			}
		});
});
