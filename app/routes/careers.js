const app = require('express').Router();
const moment = require('moment');
const randomstring = require("randomstring");
const nodemailer = require("nodemailer");
const { TRANSPORTER_OPTIONS, SENDER } = require("../../config/mailer");

const Applicant  = require('../models/applicant');

app.post('/', function(req, res){
  console.log(req.body);
  Applicant.find({email : (req.body.email).toLowerCase()}, (err, foundApplicants) =>{
    if(foundApplicants){
      foundApplicants.forEach((applicant)=>{
        if(applicant.dateOfApplication > moment().subtract(3, 'months')){
          if((applicant.position == req.body.PositionApplied) || ((applicant.position == "Associate" || applicant.position == "Senior Associate") && (req.body.PositionApplied == "Associate" || req.body.PositionApplied == "Senior Associate"))){
            console.log("Applicant already applied");
            res.send({ Success : false });
          }
        }
      })
    }
  })
  let applicant = new Applicant()
  let AppID = randomstring.generate({ length : 6, charset:'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'});
  applicant.AppID = AppID;
  applicant.dateOfApplication = Date.now();
  switch(req.body.deptType){
    case '2' : applicant.department = "IT/Software"; break;
    case '3' : applicant.department = "BPO"; break;
    case '4' : applicant.department = "HR"; break;
    case '5' : applicant.department = "Networking"; break;
    case '6' : applicant.department = "Accounts"; break;
    case '7' : applicant.department = "Admin"; break;
    case '8' : applicant.department = "Business Development"; break;
    case '9' : applicant.department = "RPO"; break;
    case '10' : applicant.department = "Marketing"; break;
  }
  applicant.position = req.body.PositionApplied;
  if(req.body.WillingnessforNightShift == "Yes") {
    applicant.nightShift = true;
  }
  else {
    applicant.nightShift = false;
  }
  let date = new Date(req.body.DateOfInterview)
  console.log("DATE" + date);
  applicant.dateOfInterview = date;
  applicant.salutation = req.body.salutation;
  applicant.firstName = titleCase(req.body.firstName);
  applicant.lastName = titleCase(req.body.lastName);
  applicant.name = applicant.firstName + ' ' + applicant.lastName;
  applicant.dob = req.body.dob;
  applicant.maritalStatus = req.body.maritalStatus;
  if(req.body.maritalStatus == "Married"){
    applicant.spouseName = titleCase(req.body.spouseName);
  }
  applicant.gender = req.body.gender;
  applicant.email = req.body.email.toLowerCase();
  applicant.number = req.body.mobile;
  applicant.phone = req.body.home_phone;
  applicant.fatherName = titleCase(req.body.fatherName);
  applicant.currentAddress.address = req.body.currentAddress;
  applicant.currentAddress.state = req.body.State;
  applicant.currentAddress.city = req.body.City;
  applicant.currentAddress.pin = req.body.pin;
  applicant.permanentAddress.address = req.body.permanentAddress;
  applicant.permanentAddress.state = req.body.permanentState;
  applicant.permanentAddress.city = req.body.permanentCity;
  applicant.permanentAddress.pin = req.body.permanentPin;
  if(req.body.previouslyWorked == "Yes") {
    applicant.workedInFusion = true;
  }
  else {
    applicant.workedInFusion = false;
  }
  applicant.we1.name = req.body.WE1CompanyName;
  applicant.we1.department = req.body.WE1Departement;
  applicant.we1.role = req.body.WE1RoleandResposibilities;
  applicant.we1.salary = req.body.WE1LastDrawnSalary;
  applicant.we1.experience = req.body.WE1ExperienceInYears;
  applicant.we2.name = req.body.WE2CompanyName;
  applicant.we2.department = req.body.WE2Departement;
  applicant.we2.role = req.body.WE2RoleandResposibilities;
  applicant.we2.salary = req.body.WE2LastDrawnSalary;
  applicant.we2.experience = req.body.WE2ExperienceInYears;
  if(req.body.appliedBefore == "Yes") {
    applicant.appliedInFusion = true;
    applicant.beforeRegId = req.body.beforeRegId;
  }
  else {
    applicant.appliedInFusion = false;
  }
  applicant.tenth.name = req.body.SchoolName10th;
  applicant.tenth.board = req.body.Board10th;
  applicant.tenth.percentage = req.body.Percentage10th;
  applicant.tenth.year = req.body.YearofPassing10th;
  applicant.twelth.name = req.body.SchoolName12th;
  applicant.twelth.board = req.body.Board12th;
  applicant.twelth.percentage = req.body.Percentage12th;
  applicant.twelth.year = req.body.YearofPassing12th;
  applicant.graduation.course = req.body.GraduationDegree;
  if( req.body.Graduation_Status_of_Completion == 'Completed' ) {
    applicant.graduation.name = req.body.GBoard;
    applicant.graduation.percentage = req.body.GPercentage;
    applicant.graduation.year = req.body.GYearofPassing;
    applicant.graduation.completed = true;
  }
  if( req.body.Graduation_Status_of_Completion == 'Results awaited') {
    applicant.graduation.semester = req.body.GCurrentSemester;
    applicant.graduation.tentative = req.body.GCurrentPassingYear;
    applicant.graduation.completed = false;
  }
  applicant.postGraduation.course = req.body.PostGraduationDegree;
  if( req.body.PostGraduation_Status_of_Completion == 'Completed' ) {
    applicant.postGraduation.name = req.body.PBoard;
    applicant.postGraduation.percentage = req.body.PPercentage;
    applicant.postGraduation.year = req.body.PYearofPassing;
    applicant.postGraduation.completed = true;
  }
  if( req.body.PostGraduation_Status_of_Completion == 'Results awaited') {
    applicant.postGraduation.semester = req.body.PCurrentSemester;
    applicant.postGraduation.tentative = req.body.PCurrentPassingYear;
    applicant.postGraduation.completed = false;
  }
  if( req.body.Ostatus != 'None') {
    applicant.otherCertification.course = req.body.otherCourses;
    if(req.body.Ostatus == 'Completed')
      applicant.otherCertification.course = true;
    if(req.body.Ostatus == 'Results awaited')
      applicant.otherCertification.course = false;
  }
  switch(req.body.SourceOfInformationType){
    case '1' : applicant.source = "Job Portals"; break;
    case '2' : applicant.source = "Newspapers"; break;
    case '3' : applicant.source = "Social Media"; break;
    case '4' : applicant.source = "Walk-In"; break;
    case '6' : applicant.source = "Campus"; break;
    case '5' : applicant.source = "Referral";
             applicant.referral.name = req.body.R1Person;
             applicant.referral.designation = req.body.R1Designation;
             applicant.referral.number = req.body.R1Contact; break;
    default : applicant.source = "Walk-In"; break;
  }
  applicant.photo = req.body.image_url;
  applicant.resume = req.body.resume_url;
  applicant.save(function(err, doc){
    if(err){
      console.log(err);
      res.send({ Success : false });
    }
    else{
      console.log("Applicant id created : " + applicant._id);

      process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
      let transporter = nodemailer.createTransport(TRANSPORTER_OPTIONS);
      let mailOptions = {
        from: SENDER , // sender address
        to: req.body.email , // list of receivers
        subject: 'Fusion Application Confirmation', // Subject line
        html: '<p>Hello ' + req.body.salutation + ' ' + req.body.firstName + ' ' + req.body.lastName + ',</p><p>Thanks for showing your interest and applying at Fusion Business Solutions Pvt. Ltd.</p><p>We have successfully received your application form!</p>You are invited to visit our office on ' + req.body.DateOfInterview + ' at 11:00 AM for the selection process, which will include an Online Test, Group Discussion and Personal Interview.<p><b>Your registration ID: <u>' + AppID + '</u> (Please bring this ID with you).</b></p><p>The venue will be:</p><p><b>Fusion Business Solutions Pvt. Ltd.</b><br>F-37, IT Park, MIA Extension,<br>Udaipur-313002,Rajasthan</p><p><b>You are kindly requested to carry a hard copy of your PAN Card/Aadhar card/ Driving License, latest resume, and salary slips (if experienced).</b></p><p><b>Note: Without above transcripts, you will not be allowed to participate in our selection process.</b></p><p>Looking forward to seeing you soon!</p>'
      };
      transporter.sendMail(mailOptions, function(error, info){
        if(error){
          console.log('error: ', error);
          res.send({ Success : false });
        }else{
          console.log("Mail Sent");
          res.setHeader('content-type','application/json')
          res.send({ Success : true });
        }
      });
    }
  });
});

function titleCase(str) {
  str = str.toLowerCase().split(' ');
  for (var i = 0; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
  }
  return str.join(' ');
}


module.exports = app;
