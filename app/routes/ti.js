const app = require('express').Router();
const moment = require('moment');

const MRF = require('../models/mrf');
const Emp = require('../models/emp');
const User = require('../models/user');
const Employee = require('../models/employee');
const Training = require('../models/training');
const Applicant = require('../models/applicant');

app.get('/dashboard', (req, res)=> {
  res.render('ti/dashboard.ejs', {
    user : req.user
  });
});

app.get('/calendar', (req, res)=> {
  Training.find({}, (err, foundTraining) => {
    res.render('ti/calendar.ejs', {
      user : req.user,
      training : foundTraining
    });
  });
});

app.get('/calendar/data', (req, res) => {
  Training.find({}, null, {sort : { start : 1}}, (err, foundTraining) => {
    console.log(foundTraining);
    res.send(foundTraining);
  })
})

app.get('/planner', (req, res)=> {
  Training.find({}, null, {sort : { start : 1}}, (err, foundTraining) => {
    Emp.find({}, null, {sort : { name : 1}}, (err, foundEmployees) => {
      res.render('ti/planner.ejs', {
        user : req.user,
        moment : moment,
        training : foundTraining,
        employee : foundEmployees
      });
    });
  });
});

app.delete('/event/delete/:id', (req, res) => {
  Training.deleteOne({_id : req.params.id}, (err, foundTraining) => {
    res.send(true);
  })
})

app.get('/event/add', (req, res) => {
  Emp.find({}, null, {sort : { name : 1}}, (err, foundEmployees) => {
    res.render('ti/event-add.ejs', {
      user : req.user,
      employee : foundEmployees
    })
  })
})

app.post('/event/add', (req, res) => {
  Emp.findOne({empId : req.body.empId}, (err, foundEmployee) => {
    let str = req.body.date;
    let pos = str.indexOf('-');
    let from = str.slice(0, pos-1);
    let to = str.slice(pos+2, str.length);
    let training = new Training();
    training.title = req.body.title;
    training.start = moment(from).format('lll');
    training.end = moment(to).format('lll');
    training.duration = moment.duration(moment(to).diff(from)).as('hours');
    training.description = req.body.description;
    training.trainer.name = foundEmployee.name;
    training.trainer.empId = req.body.empId;
    training.className = req.body.category;
    training.save(function(err, doc){
      if(err){
        res.json(err);
      }
      else{
        console.log(training._id);
        res.redirect('/ti/planner')
      }
    });
  });
});

app.get('/event/edit/:id', (req, res) => {
  Training.findOne({ _id : req.params.id}, (err, foundTraining) => {
    Emp.find({}, null, {sort : { name : 1}}, (err, foundEmployees) => {
      res.render('ti/event-edit.ejs', {
        user : req.user,
        moment : moment,
        training : foundTraining,
        employee : foundEmployees
      })
    })
  })
})

app.post('/event/edit', (req, res) => {
  Emp.findOne({empId : req.body.empId}, (err, foundEmployee) => {
    let str = req.body.date;
    let pos = str.indexOf('-');
    let from = str.slice(0, pos-1);
    let to = str.slice(pos+2, str.length);
    let training = new Training();
    training.title = req.body.title;
    training.start = moment(from).format('lll');
    training.end = moment(to).format('lll');
    training.duration = moment.duration(moment(to).diff(from)).as('hours');
    training.description = req.body.description;
    training.trainer.name = foundEmployee.name;
    training.trainer.empId = req.body.empId;
    training.className = req.body.category;
    training.save(function(err, doc){
      if(err){
        res.json(err);
      }
      else{
        console.log(training._id);
        res.redirect('/ti/planner')
      }
    });
  });
});

app.post('/event/nominee/add', (req, res) => {
  Emp.findOne({ empId : req.body.empId}, (err, foundEmployee) => {
    Training.updateOne({_id : req.body.id}, {
      $push : {
        nominees : {
          name : foundEmployee.name,
          empId : req.body.empId
        }
      }
    }, (err, result) => {
      if(err){
        res.send(err);
      }
      else {
        res.redirect('/ti/planner')
      }
    })
  })
})

module.exports = app;
