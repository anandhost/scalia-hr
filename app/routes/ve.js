const app = require('express').Router();
const request = require('request');
const moment = require('moment');

const MRF = require('../models/mrf');
const Emp = require('../models/emp');
const Pms = require('../models/pms');
const Attn = require('../models/attn');
const User = require('../models/user');
const cosec = require('../../../cosec');
const Package = require('../models/package');
const NDClient = require('../models/NDClient');
const Employee = require('../models/employee');
const Applicant = require('../models/applicant');
const NDBilling = require('../models/NDBilling');
const FusionClient = require('../models/fusionClient');
const FusionBilling = require('../models/fusionBilling');

app.get('/dashboard', function(req, res) {
  res.render('ve/dashboard.ejs', {
    user : req.user
  });
});

app.get('/resignation', function(req, res) {
  res.render('ve/resignation.ejs', {
    user : req.user
  });
});

app.get('/resource/fusion/docs', function(req, res) {
  res.render('ve/fusion-docs.ejs', {
    user : req.user
  });
});

app.get('/employee-nomination', function(req, res) {
  res.render('ve/employee-nomination.ejs', {
    user : req.user,
  });
});

app.get('/pms/score', function(req, res) {
  Emp.find({},(err, foundEmployees) =>{
    res.render('ve/pms-score.ejs', {
      user : req.user,
      moment : moment,
      employee : foundEmployees
    });
  });
});

app.post('/pms/score', (req, res)=>{
  Emp.updateOne({},{
    $set : {
      name : req.body.name,
      date : req.body.date,
      quarter : req.body.quarter,
      empId : req.body.empId,
      parameter : req.body.parameter,
      score : req.body.score
    }
  },{upsert : true}, (err, results)=>{
    if(err){
      res.json(err);
    }
    else {
      res.redirect('/ve/pms')
    }
  })
})

app.get('/pms/plan', function(req, res) {
  Emp.find({},(err, foundEmployees) =>{
    res.render('ve/pms-plan.ejs', {
      user : req.user,
      moment : moment,
      employee : foundEmployees
    });
  });
});

app.post('/pms/score', (req, res)=>{
  Emp.updateOne({},{
    $set : {
      name : req.body.name,
      empId : req.body.empId,
      date : req.body.date,
      quarter : req.body.quarter,
      existing : req.body.existing,
      expected : req.body.expected,
      commentsVA : req.body.commentsVA,
      developmentVA : req.body.developmentVA,
      developmentTL : req.body.developmentTL,
      actionPlan : req.body.actionPlan
    }
  },{upsert : true}, (err, results)=>{
    if(err){
      res.json(err);
    }
    else {
      res.redirect('/ve/pms')
    }
  })
})

app.get('/pms/report', function(req, res) {
  Emp.find({department : "VA"},(err, foundEmployees) =>{
    res.render('ve/pms-list.ejs', {
      user : req.user,
      moment : moment,
      employee : foundEmployees
    });
  });
});

app.post('/pms/report', function(req, res) {
  let qua = [];
  let quarter = req.body.quarter;
  let fromYear = req.body.year.slice(0,4)
  let toYear = req.body.year.slice(5,9)
  let from = moment().subtract(1,'days').format('DDMMYYYY');
  let to = moment().format('DDMMYYYY');
  if(quarter == "all"){
    qua = ["JUN-AUG","SEP-NOV","DEC-FEB","MAR-MAY"]
    from = '0106' + fromYear;
    to = '3105' + toYear;
  }
  else{
    qua = [quarter]
  }
  if(quarter == "JUN-AUG"){
    from = '0106' + fromYear;
    to = '3108' + fromYear;
  }
  else if(quarter == "SEP-NOV"){
    from = '0109' + fromYear;
    to = '3011' + fromYear;
  }
  else if(quarter == "DEC-FEB"){
    from = '0112' + fromYear;
    to = '2802' + toYear;
  }
  else if(quarter == "MAR-MAY"){
    from = '0103' + fromYear;
    to = '3105' + toYear;
  }
  Emp.findOne({name : req.body.name},(err, foundEmployee) => {
    Pms.find({name : req.body.name, quarter : { $in : qua }, year : req.body.year},(err, data) => {
      Emp.findOne({team : foundEmployee.team, designation : "Team Lead"},(err, teamLead) => {
        Emp.findOne({team : foundEmployee.team, designation : "Manager  VA Operations"},(err, manager) => {
          request.get(cosec.link + '/v2/attendance-daily?action=get;format=json;date-range=' + from + '-' + to + ';field-name=userid,username,firsthalf,secondhalf,latein,earlyout,punch1,punch2,worktime', {'auth': cosec.auth },(error, response, body) => {
            body = JSON.parse(body);
            console.log(data);
            res.render('ve/pms-report.ejs', {
              user : req.user,
              moment : moment,
              employee : foundEmployee,
              data : data,
              tl : teamLead,
              manager : manager,
              attendance : body["attendance-daily"],
              quarter : req.body.quarter
            })
          });
        });
      });
    });
  });
});

app.post('/pms/report/2017-2018', function(req, res) {
  let qua = [];
  let quarter = req.body.quarter;
  if(quarter == "all"){
    qua = ["JUN-AUG","SEP-NOV","DEC-FEB","MAR-MAY"]
  }
  Emp.findOne({name : req.body.name},(err, foundEmployee) => {
    Pms.find({name : { $regex : req.body.name}, quarter : { $in : qua }, year : req.body.year},(err, data) => {
      Emp.findOne({team : foundEmployee.team, designation : "Team Lead"},(err, teamLead) => {
        Emp.findOne({team : foundEmployee.team, designation : "Manager  VA Operations"},(err, manager) => {
          Attn.findOne({empID : foundEmployee.empId},(err, attendance) => {
            console.log("data : " + data);
            console.log("attn : " + attendance);
            res.render('ve/pms.ejs', {
              user : req.user,
              moment : moment,
              employee : foundEmployee,
              data : data,
              tl : teamLead,
              manager : manager,
              attendance : attendance,
              quarter : req.body.quarter
            })
          });
        });
      });
    });
  });
});

app.get('/ND/billing/add', function(req, res) {
  NDClient.find({closed : "Yes"}, null, {sort : { name : 1 }},(err, foundNDClients) => {
    Package.find({createdFor : "Noon Dalton"}, null, {sort : { code : 1}}, (err, foundPackages) => {
      res.render('ve/ND-billing.ejs', {
        user : req.user,
        package : foundPackages,
        NDClient : foundNDClients,
        messageError : req.flash('messageError'),
        messageSuccess : req.flash('messageSuccess')
      });
    });
  });
});

app.post('/ND/billing/add', (req,res) =>{
  NDClient.findOne({_id : req.body.clientId}, (err, foundNDClient) => {
    Package.findOne({_id : req.body.packageId}, (err, foundPackage) => {
      let bill = new NDBilling();
      bill.clientName = foundNDClient.name;
      bill.clientId = req.body.clientId;
      bill.occurrence = req.body.occurrence;
      bill.type = req.body.type;
      bill.startDate = req.body.startDate;
      if(bill.type == "Bonus" || bill.type == "Holiday Work Pay" || bill.type == "Holiday Discount" || bill.type == "Miscellaneous"){
        bill.amount = req.body.amount;
        if(bill.type == "Holiday Discount"){
          bill.amount = -1*bill.amount;
        }
        bill.prorated = bill.amount;
      }
      else{
        bill.packageTitle = foundPackage.title;
        bill.packageId = req.body.packageId;
        bill.clientType = req.body.clientType;
        switch (req.body.discount) {
          case "None" : bill.amount = foundPackage.amount; bill.discountType = "None"; bill.discount = 0; break;
          case "DI" : bill.amount = foundPackage.amount - (foundPackage.amount*req.body.percent)/100; bill.discountType = "Percentage Discount"; bill.discount = req.body.percent; break;
          case "FP" : bill.amount = foundPackage.amount - req.body.flat; bill.discountType = "Flat Discount"; bill.discount = req.body.flat; break;
        }
        if(req.body.type == 'Subtract Staff'){
          req.body.resources = -1*req.body.resources;
        }
        bill.resources = req.body.resources;
        if(moment(req.body.startDate).format('ll') == moment().startOf('month').format('ll')){
          bill.prorated = (req.body.resources * bill.amount).toFixed(2);
        }
        else {
          bill.prorated = ((moment().endOf('month').diff(moment(req.body.startDate), 'days') + 1) * req.body.resources * (bill.amount/30)).toFixed(2);
        }
        if(req.body.clientType == "Half Time"){
          bill.prorated = bill.prorated * 2;
        }
      }
      bill.comments = req.body.comments;
      bill.doneBy = req.body.doneBy;
      if(req.body.type == "Cancelled"){
        bill.endDate = req.body.endDate;
        bill.description = req.body.description;
      }
      bill.save((err, result) => {
        if(err){
          console.log("Error : " + err);
          req.flash('messageError','An error occured. Please try again!');
          res.redirect('/ve/ND/billing/add');
        }
        else {
          console.log("New bill created. Id is : " + bill._id);
          req.flash('messageSuccess','New bill created successfully!');
          res.redirect('/ve/ND/billing/add');
        }
      })
    })
  })
})

app.get('/fusion/billing/add', function(req, res) {
  FusionClient.find({closed : "Yes"}, null, {sort : { name : 1 }},(err, foundFusionClients) => {
    Package.find({createdFor : "Noon Dalton"}, null, {sort : { code : 1}}, (err, foundPackages) => {
      res.render('ve/fusion-billing.ejs', {
        user : req.user,
        package : foundPackages,
        fusionClient : foundFusionClients,
        messageError : req.flash('messageError'),
        messageSuccess : req.flash('messageSuccess')
      });
    });
  });
});

app.post('/fusion/billing/add', (req,res) =>{
  FusionClient.findOne({_id : req.body.clientId}, (err, foundFusionClient) => {
    Package.findOne({_id : req.body.packageId}, (err, foundPackage) => {
      let bill = new FusionBilling();
      bill.clientName = foundFusionClient.name;
      bill.clientId = req.body.clientId;
      bill.type = req.body.type;
      bill.packageTitle = foundPackage.title;
      bill.packageId = req.body.Id;
      switch (req.body.discount) {
        case "None" : bill.amount = foundPackage.amount; bill.discountType = "None"; bill.discount = 0; break;
        case "DI" : bill.amount = foundPackage.amount - (foundPackage.amount*req.body.percent)/100; bill.discountType = "Percentage Discount"; bill.discount = req.body.percent; break;
        case "FP" : bill.amount = foundPackage.amount - req.body.flat; bill.discountType = "Flat Discount"; bill.discount = req.body.flat; break;
      }
      bill.resources = req.body.resources;
      bill.startDate = req.body.startDate;
      bill.comments = req.body.comments;
      bill.doneBy = req.body.doneBy;
      if(req.body.type == "Cancelled"){
        bill.endDate = req.body.endDate;
        bill.description = req.body.description;
      }
      bill.save((err, result) => {
        if(err){
          console.log("Error : " + err);
          req.flash('messageError','An error occured. Please try again!');
          res.redirect('/ve/fusion/billing/add');
        }
        else {
          console.log("New bill created. Id is : " + bill._id);
          req.flash('messageSuccess','New bill created successfully!');
          res.redirect('/ve/fusion/billing/add');
        }
      })
    })
  })
})

module.exports = app;
