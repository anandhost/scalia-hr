const app = require('express').Router();
const moment = require('moment');

const MRF = require('../models/mrf');
const User = require('../models/user');
const Package = require('../models/package');
const NDClient = require('../models/NDClient');
const Employee = require('../models/employee');
const Applicant = require('../models/applicant');
const NDBilling = require('../models/NDBilling');
const FusionClient = require('../models/fusionClient');
const FusionBilling = require('../models/fusionBilling');

app.get('/dashboard', function(req, res) {
    res.render('manager/dashboard.ejs', {
        user : req.user
    });
});

app.get('/ND/billing/pending', isSeniorND, function(req, res){
  NDBilling.find({ approvedByManager : { $exists : false }}, (err, foundBillings) =>{
    res.render('manager/ND-billing-pending.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/ND/billing/approve/:id/:type/:clientId', isSeniorND, (req, res) => {
  NDBilling.updateOne({_id : req.params.id},{
    $set : {
      approvedByManager : "Yes",
      approvedByName : req.user.name
    }
  },
  (err, results) => {
    if(err) {
      console.log(err);
      res.send("an error occured");
    }
    else {
      if(req.params.type == "New Client" || req.params.type == "Restart"){
        NDClient.updateOne({_id : req.params.clientId},{
          $set : {
            status : "Active"
          }
        }, (err, result) => {
          if(err){
            res.send(err)
          }
          else {
            console.log("Client is now marked Active");
            res.redirect('/manager/ND/billing/pending');
          }
        })
      }
      if(req.params.type == "Cancelled" || req.params.type == "Paused"){
        NDClient.updateOne({_id : req.params.clientId},{
          $set : {
            status : "Inactive"
          }
        }, (err, result) => {
          if(err){
            res.send(err);
          }
          else {
            console.log("Client is now marked Inactive");
            res.redirect('/manager/ND/billing/pending');
          }
        })
      }
    }
  })
})

app.get('/ND/billing/reject/:id', isSeniorND, (req, res) => {
  NDBilling.updateOne({_id : req.params.id},{
    $set : {
      approvedByManager : "No",
      approvedByName : req.user.name
    }
  },
  (err, results) => {
    if(err) {
      console.log(err);
      res.send("an error occured");
    }
    else {
      res.redirect('/manager/ND/billing/pending');
    }
  })
})

app.get('/ND/billing/approved', isND, function(req, res){
  NDBilling.find({ approvedByManager : "Yes"}, (err, foundBillings) =>{
    res.render('manager/ND-billing-approved.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/ND/billing/rejected', isND, function(req, res){
  NDBilling.find({ approvedByManager : "No"}, (err, foundBillings) =>{
    res.render('manager/ND-billing-rejected.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/billing/detail', function(req, res) {
    res.render('manager/billing-detail.ejs', {
        user : req.user
    });
});

app.get('/package/create', (req,res) => {
  res.render('manager/package-create.ejs', {
    user : req.user
  })
})

app.post('/package/create', (req,res) => {
  let pack = new Package();
  pack.code = req.body.code;
  pack.title = req.body.title;
  pack.description = req.body.description;
  pack.amount = req.body.amount;
  pack.currency = req.body.currency;
  pack.createdFor = req.body.createdFor;
  pack.createdBy = req.body.createdBy;
  pack.save((err, result) => {
    if(err){
      console.log(err);
    }
    else{
      console.log("Package created :" + pack._id);
      res.redirect('/manager/package/view');
    }
  })
})

app.get('/package/view', (req,res) => {
  Package.find({}, null, {sort : {amount : 1}}, (err, foundPackages) => {
    res.render('manager/package-view.ejs', {
      user : req.user,
      package : foundPackages
    })
  })
})

app.get('/ND/client/master', isSeniorND, function(req, res){
  NDClient.find({closed : { $exists : true}}, (err, foundClients) =>{
    res.render('manager/ND-client-date.ejs', {
      user : req.user,
      moment : moment,
      client : foundClients
    })
  })
})

app.post('/ND/client/master', isSeniorND, (req, res) =>{
  NDClient.find({ date : { $gte : req.body.startDate , $lte : req.body.endDate }}, null, { sort : { _id : -1}}, (err, foundClients) =>{
    res.render('manager/ND-client-master.ejs', {
      user : req.user,
      moment : moment,
      client : foundClients
    })
  })
})

app.get('/ND/billing/master', isSeniorND, function(req, res){
  NDClient.find({closed : { $exists : true}}, (err, foundClients) =>{
    res.render('manager/ND-billing-date.ejs', {
      user : req.user,
      moment : moment,
      client : foundClients
    })
  })
})

app.post('/ND/billing/master', isSeniorND, (req, res) =>{
  let query = { startDate : { $gte : req.body.startDate , $lte : req.body.endDate }};
  if(req.body.client != "all"){
    query.clientId = req.body.client;
  }
  NDBilling.find(query, null, { sort : { _id : -1}}, (err, foundBillings) =>{
    res.render('manager/ND-billing-master.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

/////////////////////////////////////////////////////////////////////////////////
//Fusion Routes
/////////////////////////////////////////////////////////////////////////////////

app.get('/fusion/client/pipeline', isFusion, function(req, res){
  FusionClient.find({closed : { $exists : false}}, (err, foundClients) =>{
    res.render('manager/fusion-client-pipeline.ejs', {
      user : req.user,
      client : foundClients
    })
  })
})

app.get('/fusion/client/close/:id', isFusion, (req, res) => {
  FusionClient.updateOne({_id : req.params.id},{
    $set : {
      closed : "Yes",
      closedBy : req.user.name
    }
  },
  (err, results) => {
    if(err) {
      console.log(err);
      res.send("an error occured");
    }
    else {
      User.updateMany({permission : { $in : ['ve', 'ob']}},{
        $push : {
          notification : {
            title : "Client Closed. Billing can be started.",
            date : Date.now(),
            icon : "mdi mdi-account-plus",
            color : "notify-icon bg-success"
          }
        }
      }, (err, results)=> {
        if(err) {
          console.log(err);
          res.send("an error occured");
        }
        else {
          res.redirect('/manager/fusion/client/pipeline');
        }
      })
    }
  })
})

app.get('/fusion/client/cancel/:id', isFusion, (req, res) => {
  FusionClient.updateOne({_id : req.params.id},{
    $set : {
      closed : "No",
      closedBy : req.user.name
    }
  },
  (err, results) => {
    if(err) {
      console.log(err);
      res.send("an error occured");
    }
    else {
      res.redirect('/manager/fusion/client/pipeline');
    }
  })
})

app.get('/fusion/client/active', isFusion, function(req, res){
  FusionClient.find({status : "Active"}, (err, foundClients) =>{
    res.render('manager/fusion-client-active.ejs', {
      user : req.user,
      client : foundClients
    })
  })
})

app.get('/fusion/client/inactive', isFusion, function(req, res){
  FusionClient.find({status : "Inactive"}, (err, foundClients) =>{
    res.render('manager/fusion-client-inactive.ejs', {
      user : req.user,
      client : foundClients
    })
  })
})

app.get('/fusion/billing/pending', isFusion, function(req, res){
  FusionBilling.find({ approvedByManager : { $exists : false }}, (err, foundBillings) =>{
    res.render('manager/fusion-billing-pending.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/Fusion/billing/approve/:id/:type/:clientId', isFusion, (req, res) => {
  FusionBilling.updateOne({_id : req.params.id},{
    $set : {
      approvedByManager : "Yes",
      approvedByName : req.user.name
    }
  },
  (err, results) => {
    if(err) {
      console.log(err);
      res.send("an error occured");
    }
    else {
      if(req.params.type == "New Client" || req.params.type == "Restart"){
        FusionClient.updateOne({_id : req.params.clientId},{
          $set : {
            status : "Active"
          }
        }, (err, result) => {
          if(err){
            res.send(err)
          }
          else {
            console.log("Client is now marked Active");
            res.redirect('/manager/fusion/billing/pending');
          }
        })
      }
      if(req.params.type == "Cancelled" || req.params.type == "Paused"){
        FusionClient.updateOne({_id : req.params.clientId},{
          $set : {
            status : "Inactive"
          }
        }, (err, result) => {
          if(err){
            res.send(err);
          }
          else {
            console.log("Client is now marked Inactive");
            res.redirect('/manager/fusion/billing/pending');
          }
        })
      }
    }
  })
})

app.get('/fusion/billing/reject/:id', isFusion, (req, res) => {
  FusionBilling.updateOne({_id : req.params.id},{
    $set : {
      approvedByManager : "No",
      approvedByName : req.user.name
    }
  },
  (err, results) => {
    if(err) {
      console.log(err);
      res.send("an error occured");
    }
    else {
      res.redirect('/manager/fusion/billing/pending');
    }
  })
})

app.get('/fusion/billing/approved', isFusion, function(req, res){
  FusionBilling.find({ approvedByManager : "Yes"}, (err, foundBillings) =>{
    res.render('manager/fusion-billing-approved.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/fusion/billing/rejected', isFusion, function(req, res){
  FusionBilling.find({ approvedByManager : "No"}, (err, foundBillings) =>{
    res.render('manager/fusion-billing-rejected.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

function isND(req, res, next)
{
  if(req.user.subPermission == "ND" || req.user.subPermission == "Senior ND" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isFusion(req, res, next)
{
  if(req.user.subPermission == "Fusion" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isSeniorND(req, res, next)
{
  if(req.user.subPermission == "Senior ND" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

module.exports = app;
