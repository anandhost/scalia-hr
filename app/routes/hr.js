const app = require('express').Router();
const fs = require('fs');
const moment = require('moment');
const pdf = require('dynamic-html-pdf');
const nodemailer = require("nodemailer");
const { TRANSPORTER_OPTIONS, SENDER } = require("../../config/mailer");

const mongoose = require('mongoose');
const db = mongoose.connection;

const MRF        = require('../models/mrf');
const User       = require('../models/user');
const Employee   = require('../models/employee');
const Applicant  = require('../models/applicant');

app.get('/dashboard', function(req, res) {
  Applicant.find({},function(err,foundApplicants){
    MRF.find({},function(err,foundRequisitions){
      res.render('hr/dashboard.ejs', {
          user : req.user,
          moment : moment,
          applicant : foundApplicants,
          requisition : foundRequisitions
      });
    });
  });
});
// MRF Requisition create page route
//
app.get('/workforce/overview', function(req, res) {
  Applicant.find({},function(err,foundApplicants){
    MRF.find({},function(err,foundRequisitions){
      res.render('hr/workforce-overview.ejs', {
          user : req.user,
          moment : moment,
          applicant : foundApplicants,
          requisition : foundRequisitions
      });
    });
  });
});
app.get('/requisition/create', function(req, res) {
    res.render('hr/requisition-create.ejs', {
        user : req.user
    });
});

app.post('/requisition/create', function(req, res){
  let mrf = new MRF()
  mrf.department = req.body.department;
  mrf.position = req.body.position;
  mrf.number = req.body.number;
  mrf.expectedDays = req.body.expectedDays;
  mrf.replacementFor = req.body.replacementFor;
  mrf.raisedBy.name = req.body.raisedByName;
  mrf.client = req.body.client;
  mrf.required.qualification = req.body.qualification;
  mrf.required.experience = req.body.experience;
  mrf.required.competencies = req.body.competencies;
  mrf.save(function(err, doc){
    if(err){
      res.json(err);
    }
    else{
      console.log(mrf._id);
      res.redirect('/hr/requisition/create')
    }
  });
});
// MRF - requisition View route
app.get('/requisition/view', function(req, res) {
  MRF.find({approvedByHR : {$exists : false}},function(err, foundMrf){
    res.render('hr/requisition-view.ejs', {
        user : req.user,
        requisition : foundMrf
    });
  });
});

app.get('/requisition/approve/:id', function (req, res) {
  MRF.updateOne({_id : req.params.id },{
    $set : {
      approvedByHR : true
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/requisition/view');
    }
  })
});

app.get('/requisition/reject/:id', function (req, res) {
  MRF.updateOne({_id : req.params.id },{
    $set : {
      approvedByHR : false
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/requisition/view');
    }
  })
});

app.get('/requisition/upload/:id', function (req, res) {
  MRF.updateOne({_id : req.params.id },{
    $set : {
      isUploaded : true,
      uploadedOn : Date.now()
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/requisition/final');
    }
  })
});

app.get('/requisition/close/:id', function (req, res) {
  MRF.updateOne({_id : req.params.id },{
    $set : {
      isClosed : true,
      closedOn : Date.now()
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/requisition/final');
    }
  })
});

app.get('/requisition/final', function(req, res) {
  MRF.find({approvedByMD : true, isClosed : { $exists : false}},function(err, foundMrf){
    res.render('hr/requisition-final.ejs', {
        user : req.user,
        moment : moment,
        requisition : foundMrf
    });
  });
});


app.get('/requisition/all', function(req, res) {
  MRF.find({},function(err, foundMrf){
    res.render('hr/requisition-all.ejs', {
        user : req.user,
        moment : moment,
        requisition : foundMrf
    });
  });
});

// New Applicant List in table view - route
app.get('/applicant', function(req, res) {
  Applicant.find({ onlineTest : { $exists : false }}, null, {sort : { _id : -1}}, function(err,foundApplicants){
    res.render('hr/applicant.ejs', {
        user : req.user,
        moment : moment,
        applicant : foundApplicants
    });
  });
});

app.post('/applicant/remarks/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      confirmationRemarks : req.body.remarks
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/applicant');
    }
  })
});

app.get('/applicant/:id', function(req, res) {
  Applicant.findOne({_id : req.params.id},function(err,foundApplicant){
    res.render('hr/applicant-detail.ejs', {
        user : req.user,
        moment : moment,
        applicant : foundApplicant
    });
  });
});


// applicant details edit page
app.get('/applicant/edit/:id', function(req, res) {
  Applicant.findOne({_id : req.params.id},function(err,foundApplicant){
    res.render('hr/applicant-detail-edit.ejs', {
        user : req.user,
        moment : moment,
        applicant : foundApplicant
    });
  });
});

app.post('/applicant/edit/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      firstName : req.body.firstName,
      lastName : req.body.lastName,
      name : req.body.firstName + ' ' + req.body.lastName,
      dateOfInterview : req.body.dateOfInterview,
      email : req.body.email,
      department : req.body.department,
      position : req.body.position,
      number : req.body.number,
      phone : req.body.phone,
      dob : req.body.dob,
      fatherName : req.body.fatherName,
      currentAddress : {
        address : req.body.currentAddress,
        state : req.body.currentState,
        city : req.body.currentCity,
        pin : req.body.currentPin
      },
      permanentAddress : {
        address : req.body.permanentAddress,
        state : req.body.permanentState,
        city : req.body.permanentCity,
        pin : req.body.permanentPin
      },
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/applicant/edit/' + req.params.id);
    }
  })
});

// pre-interview appear table route

app.get('/interview', function(req, res){
  Applicant.find({ onlineTest : { $exists : {appeared : true}}, selected : { $exists : false}, selectedTemp : { $exists : false}} , null, {sort : { _id : -1}}, function(err, foundApplicants){
    res.render('hr/interview-table.ejs', {
      user : req.user,
      moment : moment,
      applicant : foundApplicants
    })
  })
})
// interview score and assessment route
app.get('/interview/assessment', function(req, res){
  Applicant.find({selected : null}, function(err, foundApplicants){
    res.render('hr/interview-assessment.ejs', {
      user : req.user,
      applicant : foundApplicants
    })
  })
})

app.get('/assessment/gd/:id', function(req, res){
  Applicant.findOne({_id : req.params.id }, function(err, foundApplicant){
    res.render('hr/interview-gd-score.ejs', {
      user : req.user,
      applicant : foundApplicant
    })
  })
})

app.post('/assessment/gd/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      gd : {
        appeared : true,
        skipped : false,
        m1 : {
          name : req.body.mem1name,
          verbalCommunication : req.body.mem1vc,
          listeningAbility : req.body.mem1la,
          interpersonalSkills : req.body.mem1is,
          initiative : req.body.mem1i,
          total : req.body.mem1total
        },
        m2 : {
          name : req.body.mem2name,
          verbalCommunication : req.body.mem2vc,
          listeningAbility : req.body.mem2la,
          interpersonalSkills : req.body.mem2is,
          initiative : req.body.mem2i,
          total : req.body.mem2total
        },
        m3 : {
          name : req.body.mem3name,
          verbalCommunication : req.body.mem3vc,
          listeningAbility : req.body.mem3la,
          interpersonalSkills : req.body.mem3is,
          initiative : req.body.mem3i,
          total : req.body.mem3total
        },
        average : req.body.average,
        final : req.body.final
      }
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/interview');
    }
  })
});

app.get('/assessment/gd/skip/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      gd : {
        skipped : true,
        final : 0
      }
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/interview');
    }
  })
});

app.get('/assessment/pi/:id', function(req, res){
  Applicant.findOne({_id : req.params.id }, function(err, foundApplicant){
    res.render('hr/interview-pi-score.ejs', {
      user : req.user,
      applicant : foundApplicant
    })
  })
})

app.post('/assessment/pi/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      pi : {
        appeared : true,
        m1 : {
          name : req.body.mem1name,
          commitment : req.body.mem1c,
          discipline : req.body.mem1d,
          learningAbility : req.body.mem1la,
          computerSkills : req.body.mem1cs,
          total : req.body.mem1total
        },
        m2 : {
          name : req.body.mem2name,
          commitment : req.body.mem2c,
          discipline : req.body.mem2d,
          learningAbility : req.body.mem2la,
          computerSkills : req.body.mem2cs,
          total : req.body.mem2total
        },
        m3 : {
          name : req.body.mem3name,
          commitment : req.body.mem3c,
          discipline : req.body.mem3d,
          learningAbility : req.body.mem3la,
          computerSkills : req.body.mem3cs,
          total : req.body.mem3total
        },
        average : req.body.average,
        final : req.body.final
      }
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      Applicant.findOne({_id : req.params.id}, function(err, foundApplicant){
        let total = 0, onlineTestFinal;
        if(foundApplicant.gd.skipped == true){
          onlineTestFinal = foundApplicant.onlineTest.total * 0.5;
          total = foundApplicant.pi.final + onlineTestFinal;
        }
        else {
          onlineTestFinal = foundApplicant.onlineTest.total * 0.4;
          total = foundApplicant.gd.final + foundApplicant.pi.final + onlineTestFinal;
        }
        Applicant.updateOne({_id : req.params.id },{
          $set : {
            totalScore : total,
            "onlineTest.final" : onlineTestFinal
          }},
          function(err,results){
            if(err) res.json(err);
          else {
            res.redirect('/hr/interview');
          }
        })
      })
    }
  })
});

app.post('/assessment/remarks/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      remarks : req.body.remarks
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/interview');
    }
  })
});
// interview score detail page route
app.get('/interview/score/detail/:id', function(req, res) {
  Applicant.findOne({_id : req.params.id},function(err,foundApplicants){
    res.render('hr/interview-score-detail.ejs', {
        user : req.user,
        applicant : foundApplicants
    });
  });
});

app.get('/interview/select/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      selectedTemp : true
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/interview');
    }
  })
});

app.get('/interview/reject/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      selected : false
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      Applicant.findOne({_id : req.params.id},function(err,foundApplicant){
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        let transporter = nodemailer.createTransport(TRANSPORTER_OPTIONS);
        let mailOptions = {
          from: SENDER , // sender address
          to: foundApplicant.email , // list of receivers
          subject: 'Fusion Interview Feedback', // Subject line
          html: '<p>Dear Candidate,</p><p>Greetings!</p><p>Firstly Thank you for your interest in Fusion Business Solutions Pvt. Ltd. We really appreciate it.</p><p>After the detailed discussion with you and internal discussions thereafter, we regret that we do not have a suitable opening at present that does justice to your aspirations and capabilities.</p><p>Hence we would not be able to take it forward at this juncture.We sincerely appreciate you sparing time towards discussions with us.In our growth phase, we need to work with people like you who have rich and varied experience. With your permission, we will save your details in our database and would get in touch with you, should there be a suitable opening in future. Wish you all the best for your future endeavor.</p><p>Regards,<br>Team HR</p>' // You can choose to send an HTML body instead
        };
        transporter.sendMail(mailOptions, function(error, info){
          if(error){
            res.json(error);
          }else{
            console.log("Rejection Mail Sent");
            res.redirect('/hr/interview');
          }
        });
      });
    }
  })
});

app.get('/interview/offerletter/send/fresher/:id', function(req, res) {

});

// interview approved and selected candidate list

app.get('/interview/selected', function(req, res){
  Applicant.find({selected : true}, null, {sort : { _id : -1}}, function(err, foundApplicants){
    res.render('hr/interview-selected.ejs', {
      user : req.user,
      applicant : foundApplicants
    })
  })
})

app.get('/interview/rejected', function(req, res){
  Applicant.find({selected : false}, null, {sort : { _id : -1}}, function(err, foundApplicants){
    res.render('hr/interview-rejected.ejs', {
      user : req.user,
      applicant : foundApplicants
    })
  })
})

app.get('/interview/senior', isSenior, function(req, res){
  Applicant.find({selectedTemp : true}, null, {sort : { _id : -1}}, function(err, foundApplicants){
    res.render('hr/interview-senior-select.ejs', {
      user : req.user,
      applicant : foundApplicants
    })
  })
})

app.get('/interview/senior/select/:id', isSenior, function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      selected : true,
      offerStatus : "Pending"
    }, $unset : {
      selectedTemp : ""
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/interview/selected/process/' + req.params.id);
    }
  })
});

app.get('/interview/senior/reject/:id', isSenior, function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      selected : false
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      Applicant.findOne({_id : req.params.id},function(err,foundApplicant){
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        let transporter = nodemailer.createTransport(TRANSPORTER_OPTIONS);
        let mailOptions = {
          from: SENDER , // sender address
          to: foundApplicant.email , // list of receivers
          subject: 'Fusion Interview Feedback', // Subject line
          html: '<p>Dear Candidate,</p><p>Greetings!</p><p>Firstly Thank you for your interest in Fusion Business Solutions Pvt. Ltd. We really appreciate it.</p><p>After the detailed discussion with you and internal discussions thereafter, we regret that we do not have a suitable opening at present that does justice to your aspirations and capabilities.</p><p>Hence we would not be able to take it forward at this juncture.We sincerely appreciate you sparing time towards discussions with us.In our growth phase, we need to work with people like you who have rich and varied experience. With your permission, we will save your details in our database and would get in touch with you, should there be a suitable opening in future. Wish you all the best for your future endeavor.</p><p>Regards,<br>Team HR</p>' // You can choose to send an HTML body instead
        };
        transporter.sendMail(mailOptions, function(error, info){
          if(error){
            res.json(error);
          }else{
            console.log("Rejection Mail Sent");
            res.redirect('/hr/interview/senior');
          }
        });
      });
    }
  })
});

app.get('/interview/selected/process/:id', function(req, res) {
  Applicant.findOne({_id : req.params.id},function(err,foundApplicants){
    res.render('hr/interview-selected-check.ejs', {
        user : req.user,
        moment : moment,
        applicant : foundApplicants,
        messageError : req.flash('messageError')
    });
  });
});

app.post('/interview/offerStatus/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      offerStatus : req.body.status
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/interview/selected/process/' + req.params.id);
    }
  })
});

app.get('/interview/offerletter/fresher/:id', function(req, res) {
  Applicant.findOne({_id : req.params.id},function(err,foundApplicant){
    res.render('hr/fresher-day-offerletter.ejs', {
        user : req.user,
        applicant : foundApplicant
    });
  });
});

app.post('/interview/offerletter/generate/:id', (req, res) => {
  let salary = 0;
  if (req.body.custom == 'fdsc' || req.body.custom == 'lumc' || req.body.custom == 'gbc') {
    salary = req.body.salary
  }
  if(req.body.type == 'fds') {
    salary = 10000;
    req.body.custom = 'fdsc';
  }
  if(req.body.type == 'fns') {
    salary = 10750;
    req.body.custom = 'fdsc';
  }
  let naman = {
    basic : 0,
    hra : 0,
    medical : 0,
    conveyance : 0,
    otherAllowance : 0,
    nightAllowance : 0,
    gross : 0,
    EPFEmployer : 0,
    ESICEmployer : 0,
    gratuity : 0,
    contriEmployer : 0,
    EPFEmployee : 0,
    ESICEmployee : 0,
    contriEmployee : 0,
    bonus : 0,
    critical : 0,
    ctcAnnual : 0,
    ctcMonthly : 0
  }

  if(req.body.custom == "fdsc" || req.body.type == 'fds' || req.body.type == 'fns')
  {
    if(salary > 15500){
      req.flash('messageError','Salary should be less than 15,500 in case of PF Gross')
      res.redirect('/hr/interview/selected/process/' + req.params.id);
    }
    naman.basic = 5500;
    if(req.body.type == "fns" || req.body.type =="ens"){
      naman.nightAllowance = 750;
    }
    naman.conveyance = 1600;
    naman.otherAllowance = salary - (naman.basic + naman.conveyance + naman.nightAllowance);
    naman.gross = naman.basic + naman.conveyance + naman.nightAllowance + naman.otherAllowance;
    naman.EPFEmployer = naman.basic*0.12;
    naman.ESICEmployer = naman.gross*0.0475;
    naman.gratuity = naman.basic*0.0481;
    naman.contriEmployer = naman.EPFEmployer + naman.ESICEmployer + naman.gratuity;
    naman.EPFEmployee = naman.basic*0.12;
    naman.ESICEmployee = naman.gross*0.0175;
    naman.contriEmployee = naman.EPFEmployee + naman.ESICEmployee;
    naman.bonus = (naman.gross - naman.nightAllowance)*0.0833*12;
    naman.ctcAnnual = naman.gross*12 + naman.contriEmployer*12 + naman.bonus + naman.critical;
    naman.ctcMonthly = naman.ctcAnnual/12;
  }

  if(req.body.custom == "lumc")
  {
    if(salary <= 15500 || salary > 25000){
      req.flash('messageError','Salary should be between 15,500 and 25,000 in case of Lumpsum')
      res.redirect('/hr/interview/selected/process/' + req.params.id);
    }
    naman.basic = 15500;
    if(req.body.type == "ens"){
      naman.nightAllowance = 750;
    }
    naman.conveyance = (salary - (naman.basic + naman.nightAllowance));
    if(naman.conveyance < 0){
      naman.conveyance = 0;
    }
    if(naman.conveyance > 1600){
      naman.conveyance = 1600;
    }
    naman.otherAllowance = salary - (naman.basic + naman.conveyance + naman.nightAllowance);
    if(naman.otherAllowance < 0){
      naman.otherAllowance = 0;
    }
    naman.gross = naman.basic + naman.conveyance + naman.nightAllowance + naman.otherAllowance;
    if(salary <= 21000){
      naman.ESICEmployer = naman.gross*0.0475;
      naman.ESICEmployee = naman.gross*0.0175;
    }
    naman.gratuity = naman.basic*0.0481;
    naman.contriEmployer = naman.EPFEmployer + naman.ESICEmployer + naman.gratuity;
    naman.contriEmployee = naman.EPFEmployee + naman.ESICEmployee;
    naman.bonus = (naman.gross - naman.nightAllowance)*0.0833*12;
    naman.ctcAnnual = naman.gross*12 + naman.contriEmployer*12 + naman.bonus + naman.critical;
    naman.ctcMonthly = naman.ctcAnnual/12;
  }

  if(req.body.custom == "gbc")
  {
    if(salary <= 25000){
      req.flash('messageError','Salary should be greater than 25,000 in case of Gross Based')
      res.redirect('/hr/interview/selected/process/' + req.params.id);
    }
    naman.basic = salary*0.65;
    if(req.body.type == "ens"){
      naman.nightAllowance = 750;
    }
    naman.hra = naman.basic*0.40;
    naman.conveyance = (salary - (naman.basic + naman.nightAllowance + naman.hra));
    if(naman.conveyance < 0){
      naman.conveyance = 0;
    }
    if(naman.conveyance > 1600){
      naman.conveyance = 1600;
    }
    naman.medical = salary - (naman.basic + naman.conveyance + naman.nightAllowance + naman.hra);
    if(naman.medical < 0){
      naman.medical = 0;
    }
    if(naman.medical > 1250){
      naman.conveyance = 1250;
    }
    naman.otherAllowance = salary - (naman.basic + naman.conveyance + naman.nightAllowance + naman.hra + naman.medical);
    if(naman.otherAllowance < 0){
      naman.otherAllowance = 0;
    }
    naman.gross = naman.basic + naman.conveyance + naman.nightAllowance + naman.hra + naman.medical + naman.otherAllowance;
    naman.gratuity = naman.basic*0.0481;
    naman.contriEmployer = naman.EPFEmployer + naman.ESICEmployer + naman.gratuity;
    naman.contriEmployee = naman.EPFEmployee + naman.ESICEmployee;
    naman.bonus = (naman.gross - naman.nightAllowance)*0.0833*12;
    naman.critical = (naman.gross - naman.nightAllowance)/2;
    naman.ctcAnnual = naman.gross*12 + naman.contriEmployer*12 + naman.bonus + naman.critical;
    naman.ctcMonthly = naman.ctcAnnual/12;
  }
  Applicant.findOne({_id : req.params.id}, (err, foundApplicant) => {
    res.render('hr/offerletter.ejs', {
      user : req.user,
      moment : moment,
      salary : naman,
      custom : req.body.custom,
      type : req.body.type,
      applicant : foundApplicant
    })
  })
})

app.post('/interview/offerletter/generate/fresher/:id', function(req, res) {
  Applicant.findOne({_id : req.params.id},function(err,foundApplicant){
    let salary = 146791
    if(req.body.type == 'fns' || req.body.type == 'ens' && req.body.custom == 'fds') {
      salary = 156965
    }
    else if (req.body.type == 'eds' && req.body.custom == 'lum') {
      salary = 186000
    }
    else if (req.body.type == 'ens' && req.body.custom == 'lum') {
      salary = 195000
    }
    else if (req.body.type == 'eds' && req.body.custom == 'gbf') {
      salary = 282000
    }
    else if (req.body.type == 'ens' && req.body.custom == 'gbf') {
      salary = 291000
    }
    else if (req.body.custom == 'fdsc' || req.body.custom == 'lumc' || req.body.custom == 'gbc') {
      salary = req.body.salary
    }
    let offerletter = fs.readFileSync('templates/fresher-day-offerletter.html', 'utf8');
    let options = {
      format: "A4",
      orientation: "portrait",
      border: "10mm"
    };
    let doc = {
      type: 'file',     // 'file' or 'buffer'
      template : offerletter,
      context: {
          applicant : foundApplicant,
          exp : moment().add(15, 'days').format('ll'),
          date : moment().format('ll'),
          moment : moment, //To give an expiration time of 15 days
          signature : "----",
          salary : salary
      },
      path: "./public/offerLetter/" + foundApplicant._id + ".pdf"    // it is not required if type is buffer
    };
    pdf.create(doc, options)
    res.redirect('/hr/interview/selected/process/' + req.params.id)
  });
});

app.get('/interview/offerletter/send/fresher/:id', function(req, res) {
  Applicant.findOne({_id : req.params.id},function(err,foundApplicant){
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    let transporter = nodemailer.createTransport(TRANSPORTER_OPTIONS);
    let mailOptions = {
      from: SENDER , // sender address
      to: foundApplicant.email , // list of receivers
      subject: 'Fusion Offer Letter', // Subject line
      attachments : {
        path : './public/offerLetter/' + foundApplicant._id + '.pdf'
      },
      html: '<p>You have been selected as an employee in our company.</p><p>Please find your Offer Letter in the attachments.</p>' // You can choose to send an HTML body instead
    };
    transporter.sendMail(mailOptions, function(error, info){
      if(error){
        res.json(error);
      }else{
        console.log("Mail Sent");
        res.setHeader('content-type','application/json')
        res.redirect('/hr/interview/selected/process/' + req.params.id)
      }
    });
  });
});


app.post('/interview/selected/officialUpdate/:id', function (req, res) {
  Applicant.updateOne({_id : req.params.id },{
    $set : {
      department : req.body.department,
      position : req.body.position,
      //occupation : req.body.occupation,
      //branch : req.body.branch,
      //division : req.body.division,
      confirmationDate : req.body.confirmationDate,
      joiningDate : req.body.joiningDate,
      probation : {
        from : req.body.joiningDate,
        to : moment(req.body.joiningDate).add(6,'months')
      }
    }},
    function(err,results){
      if(err) res.json(err);
    else {
      res.redirect('/hr/interview/selected/process/' + req.params.id);
    }
  })
});

// interview failed and rejected list
app.get('/interview/rejected', function(req, res){
  Applicant.find({selected : null}, function(err, foundApplicants){
    res.render('hr/interview-rejected.ejs', {
      user : req.user,
      applicant : foundApplicants
    })
  })
})
app.get('/interview/rejected/detail/:id', function(req, res) {
  Applicant.findOne({_id : req.params.id},function(err,foundApplicants){
    res.render('hr/interview-rejected-detail.ejs', {
        user : req.user,
        applicant : foundApplicants
    });
  });
});

app.get('/interview/score/report', (req, res) => {
  Applicant.distinct("dateOfInterview", (err, foundDates) => {
    Applicant.find({dateOfInterview : moment(req.query.date).format('ll'), onlineTest : { $exists : true}}, null, { sort : { totalScore : -1}}, (err, foundApplicants) => {
      Applicant.distinct("pi.m1.name", (err, foundNames) => {
        res.render('hr/interview-score-full-report.ejs', {
          user : req.user,
          moment : moment,
          date : foundDates,
          foundNames : foundNames,
          applicant : foundApplicants
        })
      })
    })
  })
})

// Generate Employee

app.get('/applicant-to-employee/:id', function (req, res) {
  Applicant.findOne({_id : req.params.id}, function(err, foundApplicant){
    console.log(foundApplicant)
    Employee.updateOne({_id : foundApplicant},
      foundApplicant,
      {upsert : true},
      function(err,results){
        if(err) res.json(err);
      else {
        Applicant.deleteOne({_id : req.params.id}, function(err, foundApplicant){
          res.redirect('/hr/employee/list');
        })
      }
    })
  })
});

app.get('/user/create', (req, res) => {
  Employee.find({username : {$exists : false}}, (err, foundEmployees) =>{
    res.render('hr/user-create.ejs', {
      user : req.user,
      employee : foundEmployees,
      errorMessage : req.flash("errorMessage"),
      successMessage : req.flash("successMessage")
    });
  });
});

app.post('/user/create', (req, res) => {
  User.findOne({ 'username' :  req.body.username }, (err, user) => {
    // if there are any errors, return the error
    if (err) {
      console.log("Error : " + err);
      req.flash('errorMessage', 'An Error Occured! Please Try Again.');
      res.redirect("/hr/user/create");
    }

    // check to see if theres already a user with that username
    if (user) {
      req.flash('errorMessage', 'That username is already taken.');
      res.redirect("/hr/user/create");
    } else {

      User.findOne({ 'email' :  req.body.email }, (err, emailid) => {

        if (err) {
          console.log("Error : " + err);
          req.flash('errorMessage', 'An Error Occured! Please Try Again.');
          res.redirect("/hr/user/create");
        }

        if (emailid) {
          req.flash('errorMessage', 'That email Id already exists.');
          res.redirect("/hr/user/create");
        } else {

        // create the user
        let newUser            = new User();

        newUser.username    = req.body.username;
        newUser.password = newUser.generateHash(req.body.password);
        newUser.email = req.body.email;
        newUser.name = req.body.name;
        newUser.permission = req.body.permission;
        newUser.subPermission = req.body.subPermission;
        newUser.emp = req.body.employeeid;
        newUser.profilePhoto = "default.png";

        newUser.save((err) => {
          if (err) {
            console.log("Error : " + err);
            req.flash('errorMessage', 'An Error Occured! Please Try Again.');
            res.redirect("/hr/user/create");
          }
          else {
            User.findOne({username : req.body.username}, (err, foundUser) =>{
              if (err) {
                console.log("Error : " + err);
                req.flash('errorMessage', 'User has been created but an error occured linking the account. Please contact IT team immediately.');
                res.redirect("/hr/user/create");
              }
              else {
                Employee.updateOne({_id : req.body.employeeid},{
                  $set : {
                    username : foundUser.username
                  }},
                  (err,results) => {
                    if (err) {
                      console.log("Error : " + err);
                      req.flash('errorMessage', 'User has been created but an error occured linking the account. Please contact IT team immediately.');
                      res.redirect("/hr/user/create");
                    }
                  else {
                    req.flash('successMessage','User created successfully! The account is ready to use.')
                    res.redirect('/hr/user/create');
                  }
                })
              }
            })
          }
        });
      }
      });
    }
  });
});

app.get('/employee/new', function(req, res){
  Applicant.find({}, null, {sort : { _id : -1}}, function(err, foundApplicants){
    res.render('hr/employee-new.ejs', {
      user : req.user,
      moment : moment,
      applicant : foundApplicants
    })
  })
})
// employee section
app.get('/employee/list', function(req, res){
  Employee.find({}, function(err, foundApplicants){
    res.render('hr/employee-list.ejs', {
      user : req.user,
      moment : moment,
      applicant : foundApplicants
    })
  })
})
//exit section
app.get('/exit/resignation', function(req, res){
  Applicant.find({}, function(err, foundApplicants){
    res.render('hr/exit-resignation-list.ejs', {
      user : req.user,
      moment : moment,
      applicant : foundApplicants
    })
  })
})
app.get('/exit/resignation/report/:id', function(req, res){
  Applicant.find({}, function(err, foundApplicants){
    res.render('hr/exit-resignation-report.ejs', {
      user : req.user,
      moment : moment,
      applicant : foundApplicants
    })
  })
})
app.get('/exit/absconded/list', function(req, res){
  Applicant.find({}, function(err, foundApplicants){
    res.render('hr/exit-absconded.ejs', {
      user : req.user,
      moment : moment,
      applicant : foundApplicants
    })
  })
})
app.get('/exit/noticeperiod', function(req, res){
  Applicant.find({}, function(err, foundApplicants){
    res.render('hr/exit-noticeperiod.ejs', {
      user : req.user,
      moment : moment,
      applicant : foundApplicants
    })
  })
})

app.get('/employee/appraisalrequest', function(req, res){
    res.render('hr/appraisalrequest.ejs', {
      user : req.user
    })
})

app.get('/employee-nominationhr', function(req, res) {
  res.render('hr/employee-nominationhr.ejs', {
      user : req.user,
  });
});

function isSenior(req, res, next)
{
  if(req.user.subPermission == "Senior" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

module.exports = app;
