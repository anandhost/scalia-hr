const app = require('express').Router();
const moment = require('moment');

const MRF = require('../models/mrf');
const User = require('../models/user');
const NDClient = require('../models/NDClient');
const Employee = require('../models/employee');
const Applicant = require('../models/applicant');

app.get('/dashboard', (req, res) => {
  res.render('ob/dashboard.ejs', {
    user : req.user
  });
});


app.get('/ND/client/pipeline', isND, function(req, res){
  NDClient.find({closed : { $exists : false}}, (err, foundClients) =>{
    res.render('ob/ND-client-pipeline.ejs', {
      user : req.user,
      client : foundClients
    })
  })
})

app.get('/ND/client/active', isND, function(req, res){
  NDClient.find({status : "Active"}, (err, foundClients) =>{
    res.render('ob/ND-client-active.ejs', {
      user : req.user,
      client : foundClients
    })
  })
})

app.get('/ND/client/inactive', isND, function(req, res){
  NDClient.find({status : "Inactive"}, (err, foundClients) =>{
    res.render('ob/ND-client-inactive.ejs', {
      user : req.user,
      client : foundClients
    })
  })
})

app.get('/ND/client/close/:id', isND, (req, res) => {
  NDClient.updateOne({_id : req.params.id},{
    $set : {
      closed : "Yes",
      closedBy : req.user.name,
      closedOn : Date.now()
    }
  },
  (err, results) => {
    if(err) {
      console.log(err);
      res.send("an error occured");
    }
    else {
      User.updateMany({permission : { $in : ['ve', 'ob']}},{
        $push : {
          notification : {
            title : "Client Closed. Billing can be started.",
            date : Date.now(),
            icon : "mdi mdi-account-plus",
            color : "notify-icon bg-success"
          }
        }
      }, (err, results)=> {
        if(err) {
          console.log(err);
          res.send("an error occured");
        }
        else {
          res.redirect('/ob/ND/client/pipeline');
        }
      })
    }
  })
})

app.get('/ND/client/cancel/:id', isND, (req, res) => {
  NDClient.updateOne({_id : req.params.id},{
    $set : {
      closed : "No",
      closedBy : req.user.name
    }
  },
  (err, results) => {
    if(err) {
      console.log(err);
      res.send("an error occured");
    }
    else {
      res.redirect('/ob/ND/client/pipeline');
    }
  })
})

//probation routes

app.get('/probation', (req, res) => {
  Employee.find({"probation.to" : { $gte : Date()}}, (err, foundEmployees) => {
    res.render('ob/probation-list.ejs', {
      user : req.user,
      moment : moment,
      employee : foundEmployees
    })
  })
})

app.post('/probation/:id', (req, res)=> {
  Employee.updateOne({_id : req.params.id },{
    $set : {
      probation : {
        to : req.body.date
      }
    }},
    (err,results) => {
      if(err) res.json(err);
    else {
      res.redirect('/ob/probation');
    }
  })
});

//induction routes
app.get('/induction',(req, res) => {
  Employee.find({},(err, foundEmployees) => {
    res.render('ob/induction-list.ejs', {
      user : req.user,
      moment : moment,
      employee : foundEmployees
    })
  })
})

app.post('/induction/:id', (req, res) => {
  Employee.updateOne({_id : req.params.id },{
    $set : {
      induction : {
        date : req.body.date,
        feedback : req.body.feedback,
        completed : true
      }
    }},
    (err,results) => {
      if(err) res.json(err);
    else {
      res.redirect('/ob/induction');
    }
  })
});

//traingin routes
app.get('/training', (req, res) => {
  Employee.find({}, (err, foundEmployees) => {
    res.render('ob/training-list.ejs', {
      user : req.user,
      moment : moment,
      employee : foundEmployees
    })
  })
})

app.post('/training/:id', (req, res) => {
  Employee.updateOne({_id : req.params.id },{
    $addToSet : {
      training : {
        topics : req.body.topics,
        date : req.body.date,
        conductedBy : req.body.conductedBy
      }
    }},
    (err,results) => {
      if(err) res.json(err);
    else {
      res.redirect('/ob/training');
    }
  })
});

app.get('/employee/assign', (req, res) => {
  res.render('ob/employee-assign.ejs', {
    user : req.user,
  });
});

app.get('/employee-nominationob', (req, res) => {
  res.render('ob/employee-nominationob.ejs', {
    user : req.user,
  });
});
//client packages routing by anand
app.get('/packages', function(req, res){
  Employee.find({"probation.to" : { $gte : Date()}}, function(err, foundEmployees){
    res.render('ob/client-package.ejs', {
      user : req.user,
      moment : moment,
      employee : foundEmployees
    })
  })
})

////////////////////////////////////////////////////////////////////////////////
// ND Routes
////////////////////////////////////////////////////////////////////////////////

app.get('/ND/client/create', (req, res) => {
  res.render('ob/client-create-ND.ejs', {
    user : req.user,
  });
});

app.post('/ND/client/create', (req, res) => {
  let client = new NDClient()
  client.name = req.body.name;
  client.jobTitle = req.body.jobTitle;
  client.email = req.body.email;
  client.number = req.body.number;
  client.officeNumber = req.body.officeNumber;
  client.company = req.body.company;
  client.address.street = req.body.street;
  client.address.city = req.body.city;
  client.address.state = req.body.state;
  client.address.country = req.body.country;
  client.address.zipCode = req.body.zipCode;
  client.website = req.body.website;
  client.service = req.body.service;
  client.package = req.body.package;
  client.startDate = req.body.startDate;
  client.startMonth = req.body.startMonth;
  client.sourceCode = req.body.sourceCode;
  client.description = req.body.description;
  client.scheduleDate = req.body.scheduleDate;
  client.scheduleTime = req.body.scheduleTime;
  client.timezone = req.body.timezone;
  client.memberName = req.body.memberName;
  client.promoCode = req.body.promoCode;
  client.date = Date.now();
  client.save((err, doc) => {
    if(err){
      console.log("Error : " + err);
      req.flash('messageError','An error occured. Please try again!');
      res.redirect('/ob/ND/client/create');
    }
    else{
      console.log("New client created. Id is : " + client._id);
      req.flash('messageSuccess','New client created successfully!');
      res.redirect('/ob/ND/client/create')
    }
  });
});

app.get('/ND/client/view', (req, res) => {
  NDClient.find({}, (err, foundClients) => {
    res.render('ob/client-view.ejs', {
      user : req.user,
      client : foundClients
    });
  });
});

app.get('/ND/client/view/detail/:id', (req, res) => {
  NDClient.findOne({_id : req.params.id}, (err, foundClient) => {
    res.render('ob/client-detail-ND.ejs', {
      user : req.user,
      moment : moment,
      client : foundClient
    });
  })
});

app.get('/ND/billing/pending', function(req, res){
  NDBilling.find({ approvedByManager : { $exists : false }}, (err, foundBillings) =>{
    res.render('ob/ND-billing-pending.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/ND/billing/approved', function(req, res){
  NDBilling.find({ approvedByManager : "Yes"}, (err, foundBillings) =>{
    res.render('ob/ND-billing-approved.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/ND/billing/rejected', function(req, res){
  NDBilling.find({ approvedByManager : "No"}, (err, foundBillings) =>{
    res.render('ob/ND-billing-rejected.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

////////////////////////////////////////////////////////////////////////////////
// Fusion Routes
////////////////////////////////////////////////////////////////////////////////


function isND(req, res, next)
{
  if(req.user.subPermission == "ND" || req.user.subPermission == "Senior ND" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

module.exports = app;
