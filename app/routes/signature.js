var app = require('express').Router();
var request = require('request');
var SendOtp = require('sendotp');

var sendOtp = new SendOtp('202847A35Sbgnl5aa8fc41','Your OTP is {{otp}}. This otp will be used to get your Digital Signatures. Please do not share it with anybody.');//Auth Key

var MRF        = require('../models/mrf');
var User       = require('../models/user');
var Employee  = require('../models/employee');
var Applicant  = require('../models/applicant');

app.get('/:id', function(req, res) {
  User.findOne({_id : req.params.id}, function(err, foundUser){
    console.log("URL : " + req.header('referer'));
    if(err){
      console.log("Error : " + err);
      req.flash('messageError','An Error Occured! Please Try Again.');
      res.redirect(req.header('referer'))
    }
    else if(!foundUser.number){

      req.flash('messageError',"The user doesn't have their phone number registered in their profile. Please add phone number and try again.");
      res.redirect(req.header('referer'))
    }
    else if(foundUser.number < 1000000000 || foundUser.number >= 10000000000){
      req.flash('messageError','The number seems to be incorrect. Please check and try again.');
      res.redirect(req.header('referer'))
    }
    else {
      var number = 910000000000 + foundUser.number;
      sendOtp.send(number, "FUSION" , function (error, data, response) {
        if(error){
          console.log("OTP Error: ");
          console.log(error);
          req.flash('messageError','An Error Occured Generating OTP! Please Try Again.');
          res.redirect(req.header('referer'))
        }
        console.log("OTP Data : ");
        console.log(data);
      });
      sendOtp.setOtpExpiry('3');
      res.redirect('/signature/otp/' + foundUser._id)
    }
  })
});

app.get('/otp/:id', function(req, res){
  res.render('otp.ejs',{
    user : req.user,
    id : req.params.id,
    messageError : req.flash('messageError'),
    url : req.header("referer")
  });
})

app.post('/otp', function(req, res){
  User.findOne({_id : req.body.id}, function(error, foundUser){
    var number = 910000000000 + foundUser.number;
    sendOtp.verify(number, req.body.otp, function (error, data, response) {
      console.log(data); // data object with keys 'message' and 'type'
      if(data.type == 'error') {
        console.log('OTP verification failed')
        req.flash('messageError',"OTP Didn't match. Try Again.");
        res.redirect('/signature/otp/' + req.body.id)
      }
      if(data.type == 'success') {
        console.log('OTP verified successfully')
        request.post(req.body.url, {
          form : {
            id : req.body.id,
            signature : foundUser.digitalSignature.signature
          }
        });
        req.flash('messageSuccess','Signature used in document successfully!')
        res.redirect('/testsign')
      }
    });
  })
})

module.exports = app;
