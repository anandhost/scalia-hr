const app = require('express').Router();
const request = require('request');
const moment = require('moment');

const MRF = require('../models/mrf');
const User = require('../models/user');
const cosec = require('../../../cosec');
const Package = require('../models/package');
const Applicant = require('../models/applicant');

app.get('/dashboard', (req, res)=> {
  Applicant.find({},(err,foundApplicants)=>{
    res.render('admin/dashboard.ejs', {
        user : req.user,
        applicant : foundApplicants
    });
  });
});

app.get('/permission/add', (req, res) => {
  User.find({ permission : { $exists : false }},(err,foundUsers)=>{
    console.log(foundUsers);
    res.render('admin/permission-add.ejs', {
        user : req.user,
        foundUsers : foundUsers
    });
  });
});

app.post('/permission/add', (req, res)=>{
  User.updateOne({ _id : req.body._id },{
    $set : {
      permission : req.body.permission
    }},
    (err, result)=>{
      if(err) {
        console.log(err);
        res.send("an error occured");
      }
      else {
        res.redirect('/admin/permission/add');
      }
    });
});
app.get('/requisition/permission',(req, res)=> {
  MRF.find({},(err, foundMrf)=>{
    res.render('admin/requisition-permission.ejs', {
        user : req.user,
        requisition : foundMrf
    });
  });
});

// MRF - requisition View route
app.get('/requisition/view', (req, res) => {
  MRF.find({approvedByHR : true, approvedByMD : { $exists : false}},(err, foundMrf)=>{
    res.render('admin/requisition-view.ejs', {
        user : req.user,
        requisition : foundMrf
    });
  });
});

app.get('/requisition/approve/:id',  (req, res) => {
  MRF.updateOne({_id : req.params.id },{
    $set : {
      approvedByMD : true
    }},
    (err,results) => {
      if(err) res.json(err);
    else {
      res.redirect('/admin/requisition/view');
    }
  })
});

app.get('/requisition/reject/:id', (req, res) => {
  MRF.updateOne({_id : req.params.id },{
    $set : {
      approvedByMD : false
    }},
    (err,results) => {
      if(err) res.json(err);
    else {
      res.redirect('/admin/requisition/view');
    }
  })
});

app.get('/attendance/daily', (req,res) => {
  request.get(cosec.link + '/v2/attendance-daily?action=get;format=json;', {
    'auth': cosec.auth
  },
  (error, response, body) => {
    if(error)
    console.log("error");
    body = JSON.parse(body);
    res.render('admin/attendance-daily.ejs',{
      attendance : body["attendance-daily"]
    });
  });
})

app.get('/attendance/event', (req,res) => {
  let currentDate = moment().format('DDMMYYYYHHMMSS')
  let pastDate = moment().subtract(1,'days').format('DDMMYYYYHHMMSS')
  request.get(cosec.link + '/v2/event-ta?action=get;format=json;date-range=' + pastDate + '-' + currentDate + ';' , {
    'auth': cosec.auth
  },
  (error, response, body) => {
    if(error){
      console.log(error);
    }
    body = JSON.parse(body);
    res.render('admin/attendance-live.ejs',{
      attendance : body['event-ta'].reverse()
    });
  });
})
// added by anand praksah
app.get('/attendance/board', (req, res)=> {
  Applicant.find({},(err,foundApplicants)=>{
    res.render('admin/attendance-view.ejs', {
        user : req.user,
        applicant : foundApplicants
    });
  });
});


module.exports = app;
