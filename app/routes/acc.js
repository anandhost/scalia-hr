const app = require('express').Router();
const moment = require('moment');

const MRF = require('../models/mrf');
const User = require('../models/user');
const NDClient = require('../models/NDClient');
const Employee = require('../models/employee');
const Applicant = require('../models/applicant');
const NDBilling = require('../models/NDBilling');
const FusionClient = require('../models/fusionClient');
const FusionBilling = require('../models/fusionBilling');

app.get('/dashboard', function(req, res) {
    res.render('acc/dashboard.ejs', {
        user : req.user,
        moment : moment
    });
});

////////////////////////////////////////////////////////////////////////////////
// ND Routes
////////////////////////////////////////////////////////////////////////////////

app.get('/ND/billing', isND, function(req, res) {
    res.render('acc/ND-billing.ejs', {
        user : req.user,
        moment : moment
    });
});

app.get('/ND/billing/current', isND, function(req, res){
  NDBilling.find({ approvedByManager : "Yes", startDate : { $gte : moment().startOf('month')}}, (err, foundBillings) =>{
    res.render('acc/ND-billing-current.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/ND/billing/all', isND, function(req, res){
  NDBilling.find({ approvedByManager : "Yes"}, (err, foundBillings) =>{
    res.render('acc/ND-billing-all.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/ND/billing/master', isSeniorND, function(req, res){
  NDClient.find({closed : { $exists : true}}, (err, foundClients) =>{
    res.render('acc/ND-billing-date.ejs', {
      user : req.user,
      moment : moment,
      client : foundClients
    })
  })
})

app.post('/ND/billing/master', isSeniorND, (req, res) =>{
  let query = { startDate : { $gte : req.body.startDate , $lte : req.body.endDate }};
  if(req.body.client != "all"){
    query.clientId = req.body.client;
  }
  NDBilling.find(query, null, { sort : { _id : -1}}, (err, foundBillings) =>{
    res.render('manager/ND-billing-master.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/ND/invoice/table', isSeniorND, function(req, res){
  res.render('acc/ND-invoice-date.ejs', {
    user : req.user,
    moment : moment
  })
})

app.post('/ND/invoice/table', isSeniorND, (req, res) =>{
  NDBilling.find({ approvedByManager : "Yes", startDate : { $gte : req.body.startDate , $lte : req.body.endDate }}, (err, foundBillings) =>{
    console.log(foundBillings);
    res.render('acc/ND-invoice-table.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

app.get('/ND/invoice/:total', function(req, res) {
    res.render('acc/ND-invoice.ejs', {
        user : req.user,
        moment : moment,
        total : req.params.total
    });
});

app.get('/ND/invoice/FBSPL', isSeniorND, function(req, res){
  NDClient.find({closed : { $exists : true}}, (err, foundClients) =>{
    res.render('acc/ND-invoice-FBSPL-date.ejs', {
      user : req.user,
      moment : moment,
      client : foundClients
    })
  })
})

app.post('/ND/invoice/FBSPL', isSeniorND, (req, res) =>{
  NDBilling.find({clientId : req.body.client, startDate : { $gte : req.body.startDate , $lte : req.body.endDate }}, (err, foundBillings) =>{
    res.render('acc/ND-invoice-FBSPL.ejs', {
      user : req.user,
      moment : moment,
      billing : foundBillings
    })
  })
})

////////////////////////////////////////////////////////////////////////////////
// Fusion Routes
////////////////////////////////////////////////////////////////////////////////

app.get('/invoice/FBSPL', function(req, res) {
    res.render('acc/invoice-fbspl.ejs', {
        user : req.user,
        moment : moment
    });
});

app.get('/invoice/FITL', function(req, res) {
    res.render('acc/invoice-fitl.ejs', {
        user : req.user,
        moment : moment
    });
});

app.get('/invoice/LLC', function(req, res) {
    res.render('acc/invoice-llc.ejs', {
        user : req.user,
        moment : moment
    });
});

function isND(req, res, next)
{
  if(req.user.subPermission == "ND" || req.user.subPermission == "Senior ND" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isFusion(req, res, next)
{
  if(req.user.subPermission == "Fusion" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isSeniorND(req, res, next)
{
  if(req.user.subPermission == "Senior ND" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

module.exports = app;
