const nodemailer = require("nodemailer");
const { TRANSPORTER_OPTIONS, SENDER } = require("../config/mailer");
const uniqueString = require('unique-string');
const moment = require('moment');
const pdf = require('dynamic-html-pdf');
const fs = require('fs');

const MRF = require('../app/models/mrf');
const Emp = require('../app/models/emp');
const User = require('../app/models/user');
const NDClient = require('../app/models/NDClient');
const Employee = require('../app/models/employee');
const Applicant = require('../app/models/applicant');

module.exports = function(app, passport) {

// normal routes ===============================================================

    // show the home page (will also have our login links)
    app.get('/', function(req, res) {
        res.redirect('/login');
    });

    app.get('/signature/add', isLoggedIn, notSigned, function(req, res) {
        res.render('signature.ejs');
    });

    app.post('/signature/add', function (req, res) {
      User.updateOne({_id : req.user._id },{
        $set : {
          digitalSignature : {
            signature : req.body.image
          }
        }},
        function(err,results){
        if(err){
          console.log("error: " + err);
          res.send(false)
        }
        else {
          res.send(true);
        }
      })
    });

    app.use('/admin', isLoggedIn, isAdmin, require('./routes/admin'))

    app.use('/profile', isLoggedIn, require('./routes/profile'))

    app.use('/manager', isLoggedIn, isManager, require('./routes/manager'))

    app.use('/hr', isLoggedIn, isHR, require('./routes/hr'))

    app.use('/ob', isLoggedIn, isOB, require('./routes/ob'))

    app.use('/acc', isLoggedIn, isACC, require('./routes/acc'))

    app.use('/ti', isLoggedIn, isTI, require('./routes/ti'))

    app.use('/ve', isLoggedIn, isVE, isLinked, require('./routes/ve'))

    app.use('/signature', isLoggedIn, require('./routes/signature'))

    app.get('/dashboard', isLoggedIn, function(req, res) {
      if(req.user.permission == "admin")
      res.redirect('/admin/dashboard');
      else if(req.user.permission == "manager")
      res.redirect('/manager/dashboard');
      else if(req.user.permission == "hr")
      res.redirect('/hr/dashboard');
      else if(req.user.permission == "ob")
      res.redirect('/ob/dashboard');
      else if(req.user.permission == "acc")
      res.redirect('/acc/dashboard');
      else if(req.user.permission == "ti")
      res.redirect('/ti/dashboard');
      else if(req.user.permission == "ve")
      res.redirect('/ve/dashboard');
      else{
        res.send("<center><h3>You don't have the user permissions to access the software yet.</h3><br> Please Mail us at hr@hostmud.com to request for the same. <a href='/logout'>Click Here</a> to go back.</center> ")
      }
    })

    // LOGOUT ==============================
    app.get('/logout', function(req, res) {
      req.session.destroy(function (err) {
        res.redirect('/');
      })
    });

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

    // locally --------------------------------
        // LOGIN ===============================
        // show the login form
        app.get('/login', function(req, res) {
            res.render('login.ejs', {
              messageError : req.flash('loginMessage'),
              messageSuccess : req.flash('loginSuccessMessage')
            });
        });

        // process the login form
        app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/dashboard', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));

        // SIGNUP =================================
        // show the signup form
        app.get('/signup', function(req, res) {
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        });

        // process the signup form
        app.post('/signup', passport.authenticate('local-signup', {
            successRedirect : '/dashboard', // redirect to the secure profile section
            failureRedirect : '/signup', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));

        //FORGET PASSWORD

        app.get('/password/forgot',function(req,res){
          res.render('password-forgot.ejs',{
            messageError : req.flash('passwordForgotError'),
            messageSuccess : req.flash('passwordForgotSuccess')});
        })

        app.post('/password/forgot', function (req, res) {
          User.findOne({ username : req.body.username }, function (err, foundUser){
            if(!foundUser||foundUser == null )
            {
              req.flash('passwordForgotError','Username doesnt exist')
              res.redirect('/password/forgot');
            }
            else {
              var key = uniqueString();
              User.updateOne({username : foundUser.username},{
                $set: {
                  reset_key : key
                }},function(err,result){
                  if(err)
                    res.json(err);
                  else {
                    console.log("reset key saved");
                  }
              })
              process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
              var transporter = nodemailer.createTransport(TRANSPORTER_OPTIONS);
              var mailOptions = {
                from: SENDER , // sender address
                to: foundUser.email , // list of receivers
                subject: 'Scalia HCM Password Reset Link', // Subject line
                html: '<div>Please use the link to change your password</div><a href="http://' + req.headers.host + '/password/reset?key='+ key +'">Click Here</a> to Change Password' // You can choose to send an HTML body instead
              };
              transporter.sendMail(mailOptions, function(error, info){
                if(error){
                  console.log('error: ', error);

                }else{
                  req.flash('passwordForgotSuccess','Mail sent successfully')
                  res.redirect('/password/forgot');
                }
              });
            }
          })
        });

        app.get('/password/reset',function(req,res){
          User.findOne({reset_key : req.query.key},function(err,foundUser){
            if(!foundUser||foundUser == null )
            {
              res.render('invalid.ejs');
            }
            else {
              res.render('password-reset.ejs', {
                user : foundUser
              });
            }
          })
        })

        app.post('/password/reset', function (req, res) {
          User.updateOne({username : req.body.username},{
            $set : {
              password : User().generateHash(req.body.password),
              reset_key : null
            }},
            function(err,results){
              if(err) res.json(err);
            else {
              req.flash('loginSuccessMessage','Password Changed');
              res.redirect('/login');
            }
          })
        });

        app.use('/careers', require('./routes/careers'))

        app.post('/applicant/verify', function(req, res){
          console.log('post request recieved');
          console.log("data is: " + req.body.data)
          console.log("data recieved is: " + req.body.regid + req.body.email)
          Applicant.findOne({AppID : req.body.regid, email : req.body.email },function(err, foundApplicant){
            if(err){
              console.log(err);
              res.send({success : false});
            }
            else {
              console.log("Found Applicant is " + foundApplicant);
              res.send({success : true, applicant : foundApplicant});
            }
          })
        })

        app.post('/applicant/test/:id/', function (req, res) {
          Applicant.updateOne({_id : req.params.id },{
            $set : {
              onlineTest : {
                appeared : true,
                total : req.body.TotalMarks
              }
            }},
            function(err,results){
              if(err) {
                console.log(err);
                res.send({Success : false});
              }
            else {
              res.send({Success : true});
            }
          })
        });

        app.get('/documents/upload', function(req, res) {
          Employee.findOne({_id : req.user.emp}, function(err, foundEmployee){
            res.render('upload-doc.ejs', {
                user : req.user,
                employee : foundEmployee
            });
          });
        });

        app.post('/documents/upload/educational/:id/', function (req, res) {
          Employee.findOne({_id : req.params.id},function(err, foundEmployee){
            var dir = './public/employee-documents/' + req.params.id ;
            if (!fs.existsSync(dir)) {
              fs.mkdirSync(dir);
            }
            req.files.marksheetMatric.mv( dir + '/Matric-Marksheet.pdf');
            req.files.marksheetInter.mv( dir + '/Inter-Marksheet.pdf');
            req.files.marksheetGrad.mv( dir + '/Graduation-Marksheet.pdf');
            req.files.marksheetPG.mv( dir + '/PostGrad-Marksheet.pdf');
            req.files.certificateGrad.mv( dir + '/Graduation-Certificate.pdf');
            req.files.certificatePG.mv( dir + '/PostGrad-Certificate.pdf');
            if(req.files.otherDegree)
            req.files.otherDegree.mv( dir + '/Other-Degree.pdf');
            Employee.updateOne({_id : foundEmployee._id },{
              $set : {
                educationalFiles : true
              }},
              function(err,results){
                if(err) {
                  res.json(err);
                }
              else {
                res.redirect('/ve/documents/upload')
              }
            })
          })
        });

        app.post('/documents/upload/previousEmployment/:id/', function (req, res) {
          Employee.findOne({_id : req.params.id},function(err, foundEmployee){
            var dir = './public/employee-documents/' + req.params.id ;
            if (!fs.existsSync(dir)) {
              fs.mkdirSync(dir);
            }
            req.files.previousAppointment.mv( dir + '/Previous-Appointment-Letter.pdf');
            req.files.previousExperience.mv( dir + '/Previous-Experience-Letter.pdf');
            req.files.previousSalarySlip.mv( dir + '/Previous-Salary-Slip.pdf');
            req.files.previousRelieving.mv( dir + '/Previous-Relieving- Letter.pdf');
            req.files.previousFinal.mv( dir + '/Previous-FnF-Letter');
            Employee.updateOne({_id : foundEmployee._id },{
              $set : {
                previousEmploymentFiles : true
              }},
              function(err,results){
                if(err) {
                  res.json(err);
                }
              else {
                res.redirect('/ve/documents/upload')
              }
            })
          })
        });

        app.post('/documents/upload/personal/:id/', function (req, res) {
          Employee.findOne({_id : req.params.id},function(err, foundEmployee){
            var dir = './public/employee-documents/' + req.params.id ;
            if (!fs.existsSync(dir)) {
              fs.mkdirSync(dir);
            }
            req.files.passportPhoto.mv( dir + '/Passport-Photo.png');
            req.files.passport.mv( dir + '/Passport.pdf');
            req.files.pan.mv( dir + '/PAN.pdf');
            req.files.aadharFront.mv( dir + '/AADHAR-Front.pdf');
            req.files.aadharBack.mv( dir + '/AADHAR-Back.pdf');
            if(req.files.dlFront)
            req.files.dlFront.mv( dir + '/DL-Front.pdf');
            if(req.files.dlBack)
            req.files.dlBack.mv( dir + '/DL-Back.pdf');
            Employee.updateOne({_id : foundEmployee._id },{
              $set : {
                panNumber : req.body.panNumber,
                aadharNumber : req.body.aadharNumber,
                personalFiles : true
              }},
              function(err,results){
                if(err) {
                  res.json(err);
                }
              else {
                res.redirect('/ve/documents/upload')
              }
            })
          })
        });

        app.get('/testsign',isLoggedIn, function(req, res) {
          User.find({},function(err,foundUsers){
            res.render('testsign.ejs', {
                user : req.user,
                users : foundUsers,
                messageError : req.flash('messageError'),
                messageSuccess : req.flash('messageSuccess')
            });
          });
        });

        app.post('/testsign', function(req, res) {
          User.findOne({_id : req.body.id},function(err,foundUser){
            if(err){
              console.log("Error : " + err);
              req.flash('messageError','An Error Occured! Please Try Again.');
              res.redirect('/testsign')
            }
            if(!foundUser || !foundUser.emp){
              req.flash('messageError','User or Employee not found.');
              res.redirect('/testsign')
            }
            Employee.findOne({_id : foundUser.emp},function(err,foundEmployee){
              if(err){
                console.log("Error : " + err);
                req.flash('messageError','An Error Occured! Please Try Again.');
                res.redirect('/testsign')
              }
              if(!foundEmployee){
                req.flash('messageError','Employee not found.');
                res.redirect('/testsign')
              }
              var salary = 146791
              var offerletter = fs.readFileSync('templates/fresher-day-offerletter.html', 'utf8');
              var options = {
                format: "A4",
                orientation: "portrait",
                border: "10mm"
              };
              var doc = {
                type: 'file',     // 'file' or 'buffer'
                template : offerletter,
                context: {
                    applicant : foundEmployee,
                    exp : moment().add(15, 'days').format('ll'),
                    date : moment().format('ll'),
                    moment : moment, //To give an expiration time of 15 days
                    signature : req.body.signature,
                    salary : salary
                },
                path: "./public/" + foundEmployee._id + ".pdf"    // it is not required if type is buffer
              };
              console.log("PDF create next");
              pdf.create(doc, options)
              req.flash('messageSuccess','Signature used in document successfully!')
              res.redirect('/testsign')
            });
          });
        });

        app.get('/knowmyemployee/:id',isLoggedIn, isAdmin, function(req, res) {
          Employee.find({_id : req.params.id},function(err,foundEmployee){
            res.render('knowmyemployee.ejs', {
                user : req.user,
                employee : JSON.stringify(foundEmployee)
            });
          });
        });

        app.get('/employee/:id',isLoggedIn,(req,res)=>{
          Emp.findOne({ empId : req.params.id},(err, foundEmployee)=>{
            res.send(foundEmployee);
          });
        });

        app.get('/teamlead/:id',isLoggedIn,(req,res)=>{
          Emp.findOne({ empId : req.params.id},(err, foundEmployee)=>{
            Emp.findOne({team : foundEmployee.team, designation : "Team Lead"},(err, teamLead)=>{
              res.send(teamLead);
            });
          });
        });

        app.get('/manager/:id',isLoggedIn,(req,res)=>{
          Emp.findOne({ empId : req.params.id},(err, foundEmployee)=>{
            Emp.findOne({team : foundEmployee.team, designation : "Manager  VA Operations"},(err, manager)=>{
              res.send(manager);
            });
          });
        });

        app.get('/ND/client/:id',isLoggedIn,(req,res)=>{
          NDClient.findOne({ _id : req.params.id},(err, foundClients)=>{
            res.send(foundClients);
          });
        });
/*
      app.use(function(req, res) {
      res.status(400);
     res.send("404 Error");
  });

  app.use(function(error, req, res, next) {
      res.status(500);
     res.send("500 Error");
   }); */

};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}

function isAdmin(req, res, next)
{
  if(req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isManager(req, res, next)
{
  if(req.user.permission == "manager" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isHR(req, res, next)
{
  if(req.user.permission == "hr" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isVE(req, res, next)
{
  if(req.user.permission == "ve" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isOB(req, res, next)
{
  if(req.user.permission == "ob" || req.user.permission == "manager" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isACC(req, res, next)
{
  if(req.user.permission == "acc" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function isTI(req, res, next)
{
  if(req.user.permission == "ti" || req.user.permission == "admin")
  return next();

  else {
    res.send("Not permitted");
  }
}

function notSigned(req, res, next)
{
  if(!req.user.digitalSignature.signature)
  return next();

  else {
    res.send("You already have your Digital Signatures Registered with us.")
  }
}

function isLinked(req, res, next)
{
  if(req.user.emp)
  return next();

  else {
    res.send("Your Account is not linked with your employee Data yet. Please contact your HR");
  }
}
