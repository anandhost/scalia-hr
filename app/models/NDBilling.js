// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var billingSchema = mongoose.Schema({
  clientName : String,
  clientId : String,
  type : String,
  packageTitle : String,
  packageId : String,
  amount : Number,
  prorated : Number,
  discountType : String,
  discount : String,
  resources : Number,
  startDate : Date,
  endDate : Date,
  doneBy : String,
  clientType : String,
  description : String,
  comments : String,
  remarksOB : String,
  remarksManager : String,
  approvedByManager : String,
  approvedByName : String,
  status : String
});

// create the model for users and expose it to our app
module.exports = mongoose.model('NDBilling', billingSchema);
