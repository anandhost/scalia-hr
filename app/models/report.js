// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var pmsSchema = mongoose.Schema({
  // New Lead
  emp : {
    name : String,
    empNo : String,
  },
  tl : {
    name : String,
    empNo : String,
  },
  manager : {
    name : String,
    empNo : String,
  },
  client : {
    name : String,
    from : Date,
    to : Date
  },
  team : String,
  email : String,
  quarter : String,

  delivery : {
    date : Date,
    score : Number,
    otd : Boolean,
    comment : String
  },
  qa : {
    date : Date,
    score : Number,
    comment : String
  },
  delivery : {
    date : Date,
    score : Number,
    feedback : String,
    type : Boolean
  },
  attendance : {

  },
  punctuality : {

  },
  retention : {

  },
  total : Number,

  performancePlan : {

  },
  developmentPlan : {

  }
});

// create the model for users and expose it to our app
module.exports = mongoose.model('PMS', pmsSchema);
