// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var NDClientSchema = mongoose.Schema({
  // New Lead
  name : String,
  jobTitle : String,
  email : String,
  number : String,
  officeNumber : String,
  company : String,
  address : {
    street : String,
    city : String,
    state : String,
    country : String,
    zipCode : Number
  },
  website : String,
  //VA (Virtual Assistant) Package
  service : String,
  package : String,
  startDate : String,
  startMonth : String,
  sourceCode : String,
  description : String,
  // scheduled Strategy and Activation Call
  scheduleDate : Date,
  scheduleTime : String,
  timezone : String,
  date : Date,
  // sales team
  memberName : String,
  promoCode : String,
  closed : String,
  closedBy : String,
  closedOn : Date,
  status : String,
});

// create the model for users and expose it to our app
module.exports = mongoose.model('NDClient', NDClientSchema);
