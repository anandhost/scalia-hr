// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
  username        : String,
  password     : String,
  name : String,
  email : String,
  number : Number,
  profilePhoto : String,
  permission : {type:String,lowercase:true,enum:["admin","manager","hr","ve","ob","acc","ti"]},
  subPermission : String,
  reset_key : String,
  emp : String,
  digitalSignature : {
    signature : String
  },
  notification : [{
    title : String,
    link : String,
    date : Date,
    icon : String,
    color : String
  }]
});

// generating a password hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
