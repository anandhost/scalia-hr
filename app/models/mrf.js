// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var mrfSchema = mongoose.Schema({
  raisedBy : {                  //requisition raised by
    name : String,
    designation : String,
    department : String
  },
  department : String, //department to be hired in
  position : String, //position to be filled
  number : Number, //number of vacancies
  replacementFor : String, //In case of resignation/termination
  expectedDays : String, //expected days to fill up
  client : String,
  required : {
    qualification : String,
    experience : String,
    competencies : String
  },
  approvedByHR : Boolean,
  approvedByMD : Boolean,
  isUploaded : Boolean,
  uploadedOn : Date,
  isClosed : Boolean,
  closedOn : Date,
  joinedOn : Date,
  empCode : String, //new employee code
  empName : String  //new employee name
});

// create the model for users and expose it to our app
module.exports = mongoose.model('MRF', mrfSchema);
