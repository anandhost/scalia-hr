// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var attendanceSchema = mongoose.Schema({
  empID : String,
  name : String,
  leaves : Number,
  totalDays : Number,
  attendance : Number,
  designation : String,
  DOJ : String
},{ collection: 'attn' },{strict : false});

// create the model for users and expose it to our app
module.exports = mongoose.model('attn', attendanceSchema);
