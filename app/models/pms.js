// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var pmsSchema = mongoose.Schema({
  // New Lead
  name : String,
  empId : String,
  date : Date,
  quarter : String,
  parameter : String,
  type : Boolean,
  entriesTotal : Number,
  entriesSample : Number,
  entriesQualified : Number,
  entriesDisqualified : Number,
  entriesNA : Number,
  score : Number,
  feedback : String,
  doneBy : String,
  year : String,
  client : String,
  existing : String,
  expected : String,
  commentsVA : String,
  developmentVA : String,
  developmentTL : String,
  actionPlan : String,
  //CR
  Month : String,
  Type : String
},
{
  strict : false
});

// create the model for users and expose it to our app
module.exports = mongoose.model('PMS', pmsSchema);
