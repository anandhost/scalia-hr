// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var billingSchema = mongoose.Schema({
  clientName : String,
  clientId : String,
  type : String,
  package : String,
  amount : Number,
  discount : String,
  resources : Number,
  startDate : Date,
  endDate : Date,
  doneBy : String,
  description : String,
  comments : String
});

// create the model for users and expose it to our app
module.exports = mongoose.model('fusionBilling', billingSchema);
