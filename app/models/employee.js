// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var employeeSchema = mongoose.Schema({

  //Data from online form
  AppID : String,
  department : String,
  position : String,
  nightShift : Boolean,
  salutation : String,
  firstName : String,
  lastName : String,
  name : String,
  dob : Date,
  maritalStatus : String,
  spouseName : String,
  gender : String,
  email : String,
  number : Number,
  phone : Number,
  fatherName : String,
  currentAddress : {
    address : String,
    state : String,
    city : String,
    pin : Number
  },
  permanentAddress : {
    address : String,
    state : String,
    city : String,
    pin : Number
  },
  workedInFusion : Boolean,
  we1 : { //Work experience
    name : String,
    department : String,
    role : String,
    salary : String,
    experience : Number,
  },
  we2 : {
    name : String,
    department : String,
    role : String,
    salary : String,
    experience : Number,
  },
  appliedInFusion : Boolean,
  beforeRegId : String,
  tenth : {
    name : String,
    board : String,
    percentage : Number,
    year : Number
  },
  twelth : {
    name : String,
    board : String,
    percentage : Number,
    year : Number
  },
  graduation : {
    name : String,
    course : String,
    percentage : Number,
    year : Number,
    completed : Boolean,
    semester : String,
    tentative : String
  },
  postGraduation : {
    name : String,
    course : String,
    percentage : Number,
    year : Number,
    completed : Boolean,
    semester : String,
    tentative : String
  },
  otherCertification : {
    course : String,
    completed : Boolean
  },
  photo : String,
  resume : String,
  source : String,
  referral : {
    name : String,
    designation : String,
    number : Number
  },
  confirmationRemarks : String,

  //Interview Information

  dateOfApplication : Date, //date of interview
  dateOfInterview : Date,
  panel : String, //Panel members taking interview
  onlineTest : {
    appeared : Boolean,
    total : Number,
    final : Number
  },
  gd : {
    appeared : Boolean,
    skipped : Boolean,
    m1 : {
      name : String,
      verbalCommunication : Number,
      listeningAbility : Number,
      interpersonalSkills : Number,
      initiative : Number,
      total : Number
    },
    m2 : {
      name : String,
      verbalCommunication : Number,
      listeningAbility : Number,
      interpersonalSkills : Number,
      initiative : Number,
      total : Number
    },
    m3 : {
      name : String,
      verbalCommunication : Number,
      listeningAbility : Number,
      interpersonalSkills : Number,
      initiative : Number,
      total : Number
    },
    average : Number,
    final : Number
  },

  pi : {
    appeared : Boolean,
    m1 : {
      name : String,
      commitment : Number,
      discipline : Number,
      learningAbility : Number,
      computerSkills : Number,
      total : Number
    },
    m2 : {
      name : String,
      commitment : Number,
      discipline : Number,
      learningAbility : Number,
      computerSkills : Number,
      total : Number
    },
    m3 : {
      name : String,
      commitment : Number,
      discipline : Number,
      learningAbility : Number,
      computerSkills : Number,
      total : Number
    },
    average : Number,
    final : Number
  },
  totalScore : Number,
  remarks : String,
  selected : Boolean,

  //Offer Extension

  offerExplanation : Boolean,
  offerSent : Boolean,
  acceptanceRecieved : Boolean,
  salaryOffered : Number,
  doj : Date, // Date of Joining

  //training

  training : {
    date : Date,
    completed : Boolean,
    conductedBy : String,
    topics : String
  },

  //Induction

  induction : {
    date : Date,
    completed : Boolean,
    feedback : String
  },

  //Official update

  occupation : String,
  branch : String,
  division : String,
  joiningDate : Date,
  confirmationDate : Date,
  probation : {
    from : Date,
    to : Date
  },
  shift : String,
  isEmployee : Boolean,
  joined : Boolean,
  // File Upload

  /*files : {
    marksheetMatric : String,
    marksheetInter : String,
    marksheetGrad : String,
    marksheetPG : String,
    certificateGrad : String,
    certificatePG : String,
    otherDegree : String,
    previousAppointment : String,
    previousExperience : String,
    previousSalarySlip : String,
    previousRelieving : String,
    previousFinal : String,
    passport : String,
    passportPhoto : String,
    pan : String,
    aadharFront : String,
    aadharBack : String,
    dlFront : String, //Driving Licence
    dlBack : String
  },*/

  panNumber : Number,
  aadharNumber : Number,
  educationalFiles : Boolean,
  previousEmploymentFiles : Boolean,
  personalFiles : Boolean




  });

// create the model for users and expose it to our app
module.exports = mongoose.model('Employee', employeeSchema);
