// load the things we need
const mongoose = require('mongoose');

// define the schema for our user model
const trainingSchema = mongoose.Schema({
  title : String,
  description : String,
  start : String,
  end : String,
  duration : String,
  className : String,
  trainer : {
    name : String,
    empId : String
  },
  nominees : [{
    name : String,
    empId : String,
    attendance : String,
    score : Number,
    feedback : String,
    total : Number
  }]
});

// create the model for users and expose it to our app
module.exports = mongoose.model('training', trainingSchema);
