var mongoose = require('mongoose');

var clientFUSSchema = mongoose.Schema({
  // New Lead
  name : String,
  title : String,
  email : String,
  number : Number,
  company : String,
  address : {
    street : String,
    city : String,
    state : String,
    country : String,
    zipCode : Number
  },
  officeNumber : String,
  website : String,
  //VA (Virtual Assistant) Package
  package : String,
  startDate : Date,
  startMonth : String,
  referralCode : String,
  // scheduled Strategy and Activation Call
  scheduleDate : Date
});

// create the model for users and expose it to our app
module.exports = mongoose.model('FusionClient', clientFUSSchema);
