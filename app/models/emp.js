// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var empSchema = mongoose.Schema({
  empId : String,
  name : String,
  attendance : String,
  department : String,
  designation : String,
  team : String,
  gender : String,
  fathersName : String,
  spouseName : String,
  dob : Date,
  joiningDate : Date,
  addressCurrent : String,
  addressPermanent : String,
  pan : String,
  email : String,
  number : Number

},{ collection: 'emp' },{strict : false});

// create the model for users and expose it to our app
module.exports = mongoose.model('emp', empSchema);
