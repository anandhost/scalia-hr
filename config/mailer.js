const  SENDER = 'Fusion Business Solutions Private Limited <hrd@fusionfirst.com>';

const TRANSPORTER_OPTIONS = {
  host : 'smtp.office365.com',
  port : 587,
  secure : false,
  tls: {
    ciphers:'SSLv3'
  },
  auth : {
    user : 'hrd@fusionfirst.com',
    pass : '@@fusion2018'
  }
};

module.exports = {
  TRANSPORTER_OPTIONS,
  SENDER
};
