## Installation & Instructions ##

* Install NodeJs from nodejs.org
* Install latest vesrion of NPM 
> sudo apt-get install npm
* Install ExpressJS
> npm install express --save
* For Database, download mongoDB from https://www.mongodb.com/download-center
* Also you can download mongoDB Compass from mongodb officical website for GUI Interface of NonSQL DB

## Run Program ##

* Clone all the files from bitbucket.
* Go to repo (fusion) folder through Terminal.
* First you have to install npm builds
> npm install
* Now you have to run app through terminal
> npm start
* App will run on localhost machine with 8080 port.
> localhost:8080

### Thanks, for any help please email me at anand@hostmud.com ###

